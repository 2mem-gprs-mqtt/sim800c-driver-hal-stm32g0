/*********************
 * INCLUDES
 *********************/
#include <at.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#ifdef MQTT_TASK
#include "cmsis_os.h"
#endif
/*********************
 * DEFINES
 *********************/
/**********************
 * TYPEDEFS
 **********************/
/**********************
 * STATIC PROTOTYPES
 **********************/
static void _vAtHandleEvent(ATInst_t * inst, AT_EVENT_e event);
static bool _bAtParseGprs(ATInst_t * inst, uint8_t c);
static bool _bAtCheckIfIsIp(ATInst_t * inst,const char* str);
static bool _bAtCheckIfIsIMSI(ATInst_t * inst,const char* str);
static bool _bAtChekSmsSending(ATInst_t * inst, RxLine_t * line, char c);
static bool _bAtWaitSms(ATInst_t * inst, const char * sms);

//RESPONCE HANDLERS
static uint8_t _ubAtGetRxSize(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtSetSignalLevel(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtGetCGATTStatus(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtGetCFUNStatus(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtGetErrorMsg(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCpin(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckGprsState(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCommon(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCusd(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCops(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCfun(ATInst_t* inst, int index, const char * input);
static uint8_t _ubAtCheckCmt(ATInst_t* inst, int index, const char * input); // Parse incoming sms
static uint8_t _ubAtCheckCmgl(ATInst_t* inst, int index, const char * input); // Parse unread SMS

static void _vAtSetGprsStatus(ATInst_t * inst, AT_GPRS_e status);
static void _vAtEraseRxLine(RxLine_t * rxLine);

/**********************
 * STATIC VARIABLES
 **********************/
AT_GENERATE_LISTS(AT_GENERATE_CPIN_LIST, CPIN, AT_)
AT_GENERATE_LISTS(AT_GENERATE_GPRS_STATUS_LIST, GPRS, AT_)
AT_GENERATE_LISTS(AT_GENERATE_RESPONCE_LIST, RESPONCE, AT_)
AT_GENERATE_LISTS(AT_GENERATE_SIM_EVENT_LIST, SIM_EVENT, AT_)
AT_GENERATE_LISTS(AT_GENERATE_CIPSTART_ANSWERS_LIST, CIPSTART,AT_)

static RxLine_t xLine;
static const char endl[] = AT_UART_EOL;
//static ATInst_t * _pxInst;
/**********************
 * MACROS
 **********************/
/**********************
 *GLOBAL FUNCTIONS
 **********************/
void vAtLoop(ATInst_t * inst){
    assert_param(inst);

    if (bAtReadLine(inst, &xLine)){
        AT_EVENT_e event = eAtHandleNewLine(inst, &xLine);
        if (event) {
            if (inst->at_mode == eAT_MODE_WAIT_ANSWER){
                inst->at_mode = eAT_MODE_COMMAND;
            }
            _vAtHandleEvent(inst, event);
        }
    }
    bAtWaitCommandMode(inst);
}

bool bAtReadLine(ATInst_t * inst, RxLine_t * line){
    assert_param(inst);
    assert_param(line);

    if (!inst) return false;

    if (dma_uart_rx_available()){
        if (bAtParseSymbol(inst, line, dma_get_symbol())) return true;
    }
    return false;
}


bool _bAtChekSmsSending(ATInst_t * inst, RxLine_t * line, char c){
    if (c == '>' && inst->eWaitStatus == eAT_WAIT_SEND_SYMBOL){
        dma_uart_send_string(inst->caSmsToSend);
        dma_uart_send_byte(AT_UART_END_MARK);
        inst->at_mode = eAT_MODE_COMMAND;
        inst->eWaitStatus = eAT_WAIT_NOTHING;
        return true;
    }
    return false;
}
bool bAtParseSymbol(ATInst_t * inst, RxLine_t * line, char c){
    assert_param(inst);
    assert_param(line);

    _bAtParseGprs(inst, c);
    if (_bAtChekSmsSending(inst, line, c)) return false;
    if (c){

        if (c==endl[line->sum]) {
            line->sum++;
        } else if (c==endl[0]) {
            line->sum = 1;
        } else {
            line->sum = 0;

            line->baBuf[line->i++] = c;

            if (line->i >= (AT_UART_LN_BUFFER_SIZE - 1)) {
                return true;
            }
        }
    }
    if(!endl[line->sum]) return true;
    return false;
}


AT_EVENT_e eAtHandleNewLine(ATInst_t * inst, RxLine_t* line){
    assert_param(inst);
    assert_param(line);

    if (!inst) return AT_ASSERT;
    line->sum = 0;
    AT_EVENT_e res = AT_UNKNOWN;

    res = eAtParseLineSystem(inst, line->baBuf);  // We should check it in any at_mode
    if (inst->at_mode != eAT_MODE_DATA) res = eAtParseLine(inst, line->baBuf); // We shouldn't check it in command mode

    _vAtEraseRxLine(line);
    return res;
}

bool bAtSendCommand(ATInst_t * inst, const char* command){
    assert_param(inst);
    assert_param(command);

    if (inst->at_mode == eAT_MODE_WAIT_ANSWER) {
        if (cCheckTimeout(&inst->xCommandTimeout)){
            inst->at_mode = eAT_MODE_COMMAND;
            cRestartTimeout(&inst->xCommandTimeout);
        }
    }

    if (inst->at_mode == eAT_MODE_DATA) {
        return bAtSwitchToCommandMode(inst, command);
    }
    if (inst->at_mode != eAT_MODE_COMMAND) return false;
    cRestartTimeout(&inst->xCommandTimeout);
    dma_uart_send_string(command);
    dma_uart_send_string(AT_UART_EOL);
    return true;
}

bool bAtSwitchToCommandMode(ATInst_t * inst, const char* command){
    assert_param(inst);
    assert_param(command);
    switch(inst->at_mode ){
        case eAT_MODE_WAIT_ANSWER:
        case eAT_MODE_COMMAND:
        case eAT_MODE_DATA:
            //do nothing
            break;
        default:
            return false;
    }
    inst->at_mode = eAT_MODE_TRANSPARENT_HOLD;
    cRestartTimeout(&inst->xTransparenTimeout);
    strlcpy(inst->caDelayedCommand, command, AT_UART_DELAYED_COMMAND_SIZE);
    return true;
}

bool bAtSwitchToDataMode(ATInst_t * inst){
    assert_param(inst);

    if (inst->at_mode != eAT_MODE_COMMAND) return false;

    bAtSendCommand(inst, "ATO\r\n");
    return true;
}
//TODO: add second timeout
bool bAtWaitCommandMode(ATInst_t * inst){
    if (inst->at_mode == eAT_MODE_TRANSPARENT_HOLD) {
        if (cCheckTimeout(&inst->xTransparenTimeout)){
            inst->at_mode = eAT_MODE_TRANSPARENT_HOLD2;
            dma_uart_send_string("+++");
            cRestartTimeout(&inst->xTransparenTimeout);

            return true;
        }
    } else if (inst->at_mode == eAT_MODE_TRANSPARENT_HOLD2){
        if (cCheckTimeout(&inst->xTransparenTimeout)){
            inst->at_mode = eAT_MODE_COMMAND;
            bAtSendCommand(inst, inst->caDelayedCommand);
            return true;
        }
    }

    return false ;
}


bool bAtInit(ATInst_t* inst, void * uart_device, void (*eventHandler)(AT_EVENT_e))
{
    assert_param(inst);
    if (!inst) return false;


    inst->at_uart = (UART_HandleTypeDef *)uart_device;
    //Init data for line catching
    rbuf_init(&inst->xRxRbuf, inst->baRxBuf, GPRS_RX_BUFFER_SIZE);
    rbuf_init(&inst->xTxRbuf, inst->baTxBuf, GPRS_TX_BUFFER_SIZE);
    dma_uart_init(uart_device, AT_UART_SEND_TIMEOUT_MS);
    dma_uart_send_byte(AT_UART_END_MARK);

    //Status
    vAtResetStatus(inst);

    inst->at_event_handler = eventHandler;
    return true;
}

void vAtResetStatus(ATInst_t* inst){
    if (!inst) return;

    cStartTimeout(&inst->xTxTimeout, AT_UART_TX_TIMEOUT);
    inst->usTcpRxLength = 0;
    inst->usTcpRxAvailable = 0;
    inst->xTxAvailable = 0;
    inst->eGatt = eAT_CGATT_UNKNOWN;
    inst->eCfun = eAT_CFUN_UNKNOWN;
    inst->ubRssi = 0;
    inst->ubBer = 0;

    inst->ePin = eAT_CPIN_UNKNOWN;
    inst->eGprs = eAT_GPRS_CLOSED;
    inst->at_mode = eAT_MODE_COMMAND;
    memset(xLine.baBuf, 0, AT_UART_LN_BUFFER_SIZE);

    rbuf_flush(&inst->xRxRbuf);
    rbuf_flush(&inst->xTxRbuf);
    xLine.i = 0;
    xLine.sum = 0;
    inst->eLastError = SIM800_ERR_NULL;
    inst->bIsConnected = false;
    inst->caDelayedCommand[0] = 0;
    inst->iBalance = 0;
    inst->eOpDefinedCode = eNULL_OPERATOR; //TODO: should be -1 on start
    inst->uiCops = 0;
    inst->xOperator = getOperator(0);
    inst->eWaitStatus = eAT_WAIT_NOTHING;
    cStartTimeout(&inst->xCommandTimeout, AT_UART_SEND_TIMEOUT_MS);
    cStartTimeout(&inst->xTransparenTimeout, AT_UART_TRANSPARENT_TIMEOUT);
}


AT_CPIN_e eAtGetCPIN(ATInst_t * inst){
    return inst->ePin;
}

AT_GPRS_e eAtGetGprsState(ATInst_t * inst){
    return inst->eGprs;
}

AT_EVENT_e eAtParseLineSystem(ATInst_t* inst, const char * input){
    assert_param(inst);
    assert_param(input);

    if (strstr(input, AT_CIPSTART_STRINGS[eAT_CIPS_PDP_DEACT])){
        inst->bIsConnected = false;
        return AT_UNKNOWN;
        return AT_DISONNECTED;
    } else if (strstr(input, AT_CIPSTART_STRINGS[eAT_CIPS_POWER_DOWN])){
        inst->bIsConnected = false;
        return AT_POWEROFF;
    }
    return AT_UNKNOWN;
}

AT_EVENT_e eAtParseLine(ATInst_t * inst, char * line){
    assert_param(inst);
    assert_param(line);


    char * ret;

    if (_bAtWaitSms(inst, line)) return AT_GOT_SMS;
    for (int i = 0 ; i <  AT_RESPONCE_SIZE; i++){
        ret = strstr(line, AT_RESPONCE_STRINGS[i]);
        if (ret && AT_RESPONCE_CBS[i]){
            return AT_RESPONCE_CBS[i](inst, i, ret);
        }
    }

    return AT_UNKNOWN;
}

AT_EVENT_e eAtCheckATV(const char * line){
    if (strlen(line) == 1){
        char val = line[0] - '0';
        switch (val){
        case eATV_OK:  return AT_OK;
        case eATV_ERROR: return AT_ERROR;
        case eATV_CONNECT: return AT_CONNECTED;
        default:
            return AT_UNKNOWN;
        }
    }
    return AT_UNKNOWN;
}

bool validateIP4Dotted(const char *s, uint32_t * ip)
{
    assert_param(s);
    assert_param(ip);

    int len = strlen(s);

    if (len < 7 || len > 15)
        return false;

    char tail[16];
    tail[0] = 0;

    unsigned int d[4];
    int c = sscanf(s, "%3u.%3u.%3u.%3u%s", &d[0], &d[1], &d[2], &d[3], tail);


    if (c != 4 || tail[0])
        return false;

    for (int i = 0; i < 4; i++)
        if (d[i] > 255)
            return false;
    uint8_t * answer = (uint8_t*)ip;
    answer[0] = d[3];
    answer[1] = d[2];
    answer[2] = d[1];
    answer[3] = d[0];
    return true;
}
/**********************
 *STATIC FUNCTIONS
 **********************/

void _vAtEraseRxLine(RxLine_t * line){
    assert_param(line);

    memset(line->baBuf, 0, AT_UART_LN_BUFFER_SIZE);
    line->i = 0;
}


void _vAtHandleEvent(ATInst_t * inst, AT_EVENT_e event){
    assert_param(inst);
    assert_param(inst->at_event_handler);

    inst->at_event_handler(event);
}

bool _bAtParseGprs(ATInst_t * inst, uint8_t c){
    assert_param(inst);

    if (inst->at_mode == eAT_MODE_DATA){
        if (!rbuf_push(&inst->xRxRbuf, c)){
            //TODO: how to handle rbuf overflow here?
            return true;
        }
    }
    return false;
}

bool _bAtCheckIfIsIp(ATInst_t * inst, const char* str){
    assert_param(inst);

    uint32_t answer;

    if (!validateIP4Dotted(str, &answer)) {
        return false;
    } else {
        inst->ulIp = answer;
        return true;

    }
    return false;
}

//TODO: Add eWait story
static bool _bAtCheckIfIsIMSI(ATInst_t * inst, const char* str){

    if (strlen(str) == AT_IMSI_LEN){
        strlcpy(inst->caIMSI, str, AT_IMSI_LEN+1);
        char mcc_mnc[5];
        strncpy(mcc_mnc, str,5);
        const GSMOperator_x * oper = getOperator(atoi(mcc_mnc));
        if (oper) {

            inst->xOperator = oper;
            inst->eOpDefinedCode = oper->eOp;
            return true;
        }
    }
    return false;
}

uint8_t _ubAtGetRxSize(ATInst_t* inst, int index, const char * input){
    assert_param(inst);
    assert_param(input);

    int mode;
    int size;
    int res;
    int ulRxSize;
    if ((res=sscanf(input, "+CIPRXGET: %d,%d,%d", &mode, &size, &ulRxSize)) == 3){
        if (ulRxSize < 0) {
            ulRxSize = 0;
        }
        inst->usTcpRxLength = size;
        inst->usTcpRxAvailable =ulRxSize;
        return AT_INCOMING_DATA;
    } else if ((res = sscanf(input, "+CIPRXGET: %d,%d", &mode, &size)) == 2){
        // It is just an echo, do nothing
        return AT_UNKNOWN;
    } else if ((res = sscanf(input, "+CIPRXGET: %d", &size)) == 1){
        inst->usTcpRxAvailable = 1;
        return AT_INCOMING_DATA;
    }
    return res;
}
uint8_t _ubAtSetSignalLevel(ATInst_t* inst,int index, const char * input){
    assert_param(inst);
    assert_param(input);

    if (!inst) return AT_ASSERT;

    int rssi;
    int ber;
    int res = sscanf(input, "+CSQ: %d,%d", &rssi, &ber);
    if (res >= 1){
        inst->ubRssi =((rssi >= 0) && (rssi < 100))? rssi: 0;
        inst->ubBer = ber >= 0 && ber < 100 ? ber: 0;
        return AT_GOT_STATUS;
    }
    return AT_UNKNOWN;//simple_ts_log("Signal level has came\r\n");
}

// Check CPIN state
uint8_t _ubAtCheckCpin(ATInst_t* inst, int index, const char * input){
    assert_param(inst);
    assert_param(input);

    if (!inst) return AT_ASSERT;

    for (int i = 0; i < AT_CPIN_SIZE; i++){
        if (AT_CPIN_STRINGS[i] && strstr(input, AT_CPIN_STRINGS[i])){
            inst->ePin = i;
            return AT_GOT_STATUS;
        }
    }
    return AT_UNKNOWN;
}


//TODO: We should not call events here
void _vAtSetGprsStatus(ATInst_t * inst, AT_GPRS_e status){
    assert_param(inst);

    inst->eGprs = status;
    if (inst->eGprs == eAT_GPRS_CLOSED) _vAtHandleEvent(inst, AT_DISONNECTED);
}

// Check GPRS state
uint8_t _ubAtCheckGprsState(ATInst_t* inst, int index, const char * input){
    assert_param(inst);
    assert_param(input);

    for (int i = 0; i < AT_GPRS_SIZE; i++){
        if (strstr(input, AT_GPRS_STRINGS[i])){
            _vAtSetGprsStatus(inst, i);
            return AT_GOT_STATUS;
        }
    }
    return AT_UNKNOWN;
}


uint8_t _ubAtCheckCommon(ATInst_t* inst, int index, const char * input){
    if (!inst) return AT_ASSERT;

    int res;
    if ((res = eAtCheckATV(input))) return res;


    if (strstr(input, AT_CIPSTART_STRINGS[eAT_CIPS_CONNECT_FAIL])){
        inst->bIsConnected = false;
        return AT_ERROR;
    } else if (strstr(input, AT_CIPSTART_STRINGS[eAT_CIPS_CONNECT_OK])){
        inst->at_mode = eAT_MODE_DATA;
        inst->bIsConnected = true;
        return AT_CONNECTED;
    } else if (strstr(input, AT_CIPSTART_STRINGS[eAT_CIPS_ALREADY_CONNECT])){
        inst->bIsConnected = true;
        return AT_CONNECTED;
    } else if (strstr(input, "CLOSED")){
        inst->bIsConnected = false;
        return AT_DISONNECTED;
    } else if(strstr(input, "SEND OK")){
        return (AT_SEND_OK);
    } else if (strstr(input, "OK")) {
        return AT_OK;
    } else if(strstr(input, "ERROR")){
        return (AT_ERROR);
    } else if (_bAtCheckIfIsIp(inst, input)) {
        return AT_OK;
    } else if (_bAtCheckIfIsIMSI(inst, input)) {
        return AT_GOT_OPERATOR;
    } else if (_bAtWaitSms(inst, input)){
        return AT_GOT_SMS;
    }
    return AT_UNKNOWN;
}


uint8_t _ubAtGetCGATTStatus(ATInst_t* inst, int index, const char * input){
    if (!inst) return AT_ASSERT;

    int val = 0;
    int res = sscanf(input, "+CGATT: %d\r\n", &val);
    if (res == 0) {
        inst->eGatt = eAT_CGATT_UNKNOWN;
        return AT_UNKNOWN;
    }
    switch (val){
    case eAT_CGATT_ATTACHED:
    case eAT_CGATT_DETACHED:
    case eAT_CGATT_UNKNOWN:
        inst->eGatt = val;
        return AT_GOT_STATUS;
    default:
        inst->eGatt = eAT_CGATT_UNKNOWN;
        return AT_UNKNOWN;
    }
}

uint8_t _ubAtGetCFUNStatus(ATInst_t* inst, int index, const char * input){
    if (!inst) return AT_ASSERT;

    int val = 0;
    int res = sscanf(input, "+CFUN: %d\r\n", &val);
    if (res == 0) {
        inst->eCfun = eAT_CGATT_UNKNOWN;
        return res;
    }
    switch (val){
        case eAT_CFUN_MIN:
        case eAT_CFUN_FULL:
        case eAT_CFUN_DIS:
            inst->eCfun = val;
            break;
        default:
            inst->eCfun = eAT_CFUN_UNKNOWN;
        }
    return res;
}

static uint8_t _ubAtGetErrorMsg(ATInst_t* inst, int index, const char * input){
    if (!inst) return AT_ASSERT;

    int val;
    int res = sscanf(input, "+CME ERROR: %d\r\n", &val);
    if (res) {
        //TODO: check if VAL belongs to error enum
        inst->eLastError = val;
        //p->at_event_handler(AT_ERROR);
        return AT_CME_ERROR;
    }
    return AT_ERROR;

}

static uint8_t _ubAtCheckCusd(ATInst_t* inst, int index, const char * input){
    if (!inst) return AT_ASSERT;

    int n = 0; //A numeric parameter which indicates control of the unstructured supplementary service data whatever that means
    char message[AT_INCOMING_USSD_DATA]; //= malloc(AT_INCOMING_USSD_DATA);
    int dcs = 0; // Cell Broadcast Data Coding Scheme in integer format

    sscanf(input, "+CUSD: %d, \"%s\",\", %d", &n, message, &dcs);

    //try to parce balance
    int bal = 0;
    if (USC2ParseBalance(message, inst->eOpDefinedCode, &bal) == 0){
        inst->iBalance = bal;
        return AT_BALANCE_CAME;
    }
    return AT_CUSD_CAME;
}

static uint8_t _ubAtCheckCops(ATInst_t* inst, int index, const char * input){
    int mode;
    int format;
    int cops;

    if (sscanf(input, "+COPS: %d,%d,\"%d\"", &mode, &format, &cops) == 3){
        inst->uiCops = cops;
        return AT_GOT_STATUS;
    }

    return AT_UNKNOWN;
}

static uint8_t _ubAtCheckCfun(ATInst_t* inst, int index, const char * input){
    int fun;

    if (sscanf(input, "+CFUN: %d", &fun) == 1){
        inst->eCfun = fun;
        return AT_GOT_STATUS;
    }

    return AT_UNKNOWN;
}

bool bAtSendSms(ATInst_t * inst, const char * sms){
    if (inst->eWaitStatus == eAT_WAIT_NOTHING) {
        strncpy(inst->caSmsToSend, sms, AT_MAX_SMS_LEN);
        inst->eWaitStatus = eAT_WAIT_SEND_SYMBOL;
        return true;
    }
    return false;
}

bool _bAtWaitSms(ATInst_t * inst, const char * sms){
    if (inst->eWaitStatus == eAT_WAIT_SMS_INCOMING) {
        strlcpy(inst->cxSms.msg, sms, AT_MAX_SMS_LEN);
        inst->eWaitStatus = eAT_WAIT_NOTHING;
        return true;
    }
    return false;
}
static uint8_t _ubAtCheckCmt(ATInst_t* inst, int index, const char * input){
    char number[AT_PHONE_LENGTH];
    char field[16];
    char date[AT_DATE_LENGTH];

    // Number:
    char * start = strchr(input, '\"');
    char * end = strchr(++start,'\"');
    if (!start) return AT_UNKNOWN;
    if (!end) return AT_UNKNOWN;
    strlcpy(number, start, end-start+1);
    start = strchr(++end, '\"');
    end = strchr(++start, '\"');
    if (!start) return AT_UNKNOWN;
    if (!end) return AT_UNKNOWN;
    strlcpy(field, start, end-start+1);
    start = strchr(end, '\"');
    end = strchr(++start, '\"');
    if (!start) return AT_UNKNOWN;
    if (!end) return AT_UNKNOWN;
    strlcpy(date, start, end-start+1);
    inst->eWaitStatus = eAT_WAIT_SMS_INCOMING;
    strlcpy(inst->cxSms.number, number, AT_PHONE_LENGTH);
    strlcpy(inst->cxSms.date, date, AT_DATE_LENGTH);
    return AT_GOT_STATUS;
   /* if (sscanf(input, "+CMT: \"%s\",\"%s\",\"%s\"", number, field, date) == 3){
        strlcpy(inst->cxSms.number, number, AT_PHONE_LENGTH);
        strlcpy(inst->cxSms.date, date, AT_DATE_LENGTH);
        inst->eWaitStatus = eAT_WAIT_SMS_INCOMING;
        return AT_GOT_STATUS;
    }*/

    return AT_UNKNOWN;
}

uint8_t _ubAtCheckCmgl(ATInst_t* inst, int index, const char * input){
    int count = 0;
    char number[AT_PHONE_LENGTH];
    char field[16];
    char date[AT_DATE_LENGTH];

    if (sscanf(input, "+CMGL: %d,\"REC UNREAD\"\"%s\",\"%s\",\"%s\"", &count,number, field, date) == 4){
        strlcpy(inst->cxSms.number, number, AT_PHONE_LENGTH);
        strlcpy(inst->cxSms.date, date, AT_DATE_LENGTH);
        inst->eWaitStatus = eAT_WAIT_SMS_INCOMING;
        return AT_GOT_STATUS;
    }

    return AT_UNKNOWN;
}
/**********************
 *  TEST FUNCTIONS
 **********************/
bool test_bAtReadSymbol(ATInst_t * inst){
    return bAtReadLine(inst, &xLine);
}
uint8_t test_vAtGetCGATTStatus(ATInst_t* p, int index, char * input){
    return _ubAtGetCGATTStatus( p, index, input);
}
uint8_t test_vAtGetRxSize(ATInst_t* p, int index, const char * input){
    if (!p) return 0;
    return _ubAtGetRxSize(p, index, input);
}

uint8_t test_ubAtGetCFUNStatus(ATInst_t* p, int index, const char * input){
    if (!p) return 0;
    return _ubAtGetCFUNStatus(p, index, input);
}

uint8_t test_ubAtSetSignalLevel(ATInst_t* p,int index, const char * input){
    return _ubAtSetSignalLevel(p, index, input);
}

ATInst_t * test_pxGetInst(){
    return 0;
}

bool test_bAtCheckIfIsIp(ATInst_t * inst, char* str){
    return _bAtCheckIfIsIp(inst, str);
}

