/*********************
 * INCLUDES
 *********************/
#include <GSM.h>


#ifdef MQTT_TASK
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "semphr.h"
#endif
/*********************
 * DEFINES
 *********************/
/**********************
 * TYPEDEFS
 **********************/
/**********************
 * STATIC PROTOTYPES
 **********************/
void _vGSMCheckOk(AT_EVENT_e event);
void _vGSM_ATHandler(AT_EVENT_e event);
bool _bGSMDoSequence(GSMSequence_t * seq);

/**
 * @brief: send command with response;
 * @param command: command's enum to send;
 * @return true, if OK came, false otherwise.
 *
 * There is a state-machine inside to wait the responce
 */
bool _bGSMSendCommWithResp(GSM_COMMANDS_e command);

bool _bGSMCheckSoftReset(GSMHandler_t * inst);
bool _bGSMCheckCfun(GSMHandler_t * inst);
bool _bGSMCheckCpin(GSMHandler_t * inst);
bool _bGSMCheckCimi(GSMHandler_t * inst);
bool _bGSMCheckCgatt(GSMHandler_t * inst);
bool _bGSMCheckConnection(GSMHandler_t * inst);
bool _bGSMCheckRssi(GSMHandler_t * inst);

// Routines for different states:
void _vGSMLoopPowering();
/**********************
 * STATIC VARIABLES
 **********************/
static GSMHandler_t _xInst;
static GSMTimeout_t _xTimer;
static uint32_t _uiErrorAmount = 0;
AT_GENERATE_LISTS(GSM_GENERATE_ZERO_ARGUMENT_COMMAND_LIST, COMMANDS, _GSM_)

static GSMSequence_t _xAtSettingsSequence = {
        .commands = {\
                     eGSM_COMM_UART, \
                     eGSM_COMM_ATE, \
                     eGSM_COMM_ATV, \
                     eGSM_COMM_ERR_LVL, \
        },
        .size = 4,
        .i = 0
};

static GSMSequence_t _xInitSequence = {
        .commands = {\
                     eGSM_COMM_CNMI, \
                     eGSM_COMM_CMGF, \
                     eGSM_COMM_CSCS, \
                     eGSM_COMM_CLIP, \
                     eGSM_COMM_REJECT_CALLS, \
                     eGSM_COMM_GET_BALANCE, \
        },
        .size = 6,
        .i = 0
};

static GSMSequence_t _xJoinSequence = {
        .commands = {eGSM_COMM_CIPMODE, \
                     eGSM_COMM_RXGET, \
                     eGSM_COMM_CSTT, \
                     eGSM_COMM_CIICR, \
                     eGSM_COMM_CIFSR},
        .size = 5,
        .i = 0
};

/**********************
 * MACROS
 **********************/

/**********************
 * GLOBAL FUNCTIONS
 **********************/
void vGSMLoop(){
    vAtLoop(&_xInst.xAt);

    // Check device power status
    _vGSMLoopPowering();

    switch(_xInst.eStatus){
    case eGSM_POWERING: ; break; //Do nothin, beacause it's already handled
    case eGSM_UART_SETUP:
        if (_xInst.eCommandState == eGSM_COMMAND_CHECK){
            if (cCheckTimeout(&_xTimer)){
                if (bAtSwitchToCommandMode(&_xInst.xAt, _GSM_COMMANDS_STRINGS[_xInst.eCurrentCommand]))
                    cRestartTimeout(&_xTimer);
            }
        }
        if (_bGSMDoSequence(&_xAtSettingsSequence)) {
            GSM_LOG("Sim is ready\r\n");
            _xInst.eStatus = eGSM_INITING;
           // if (eAtGetCPIN(&_xInst.xAt) == eAT_CPIN_READY) _xInst.eStatus = eGSM_INITING;
        }
        break;
    case eGSM_INITING:
        if (!_bGSMCheckSoftReset(&_xInst)) break;
        if (!_bGSMCheckCfun(&_xInst)) break;
        if (!_bGSMCheckCpin(&_xInst)) break;
        if (!_bGSMCheckRssi(&_xInst)) break;
        if (!_bGSMCheckCimi(&_xInst)) break;
        if (_bGSMDoSequence(&_xInitSequence)){
            _xInst.eStatus = eGSM_JOINING;
            GSM_LOG("RSSI: %d\r\n", _xInst.xAt.ubRssi);
        }
        break;
    case eGSM_JOINING:
        if (!_bGSMCheckCgatt(&_xInst)) break;
        if (_bGSMDoSequence(&_xJoinSequence) ){
            GSM_LOG("Got ip: %s\r\n", bGSMGetIPAddress());
            _xInst.eStatus = eGSM_CONNECTING;
        }
        break;
    break;
    case eGSM_CONNECTING:
        if (_bGSMSendCommWithResp(eGSM_COMM_CONNEC)) {
            _xInst.eStatus = eGSM_CONNECTING2;
            cStartTimeout(&_xInst.xConnTimeout, GSM_CONNECTION_TIMEOUT);
        }
        break;
    case eGSM_CONNECTING2:
        if (!_bGSMCheckConnection(&_xInst)) break;
        _xInst.eStatus = eGSM_IDLE;
        _xInst.onConnect();
        rbuf_flush(&_xInst.xAt.xRxRbuf);
        rbuf_flush(&_xInst.xAt.xTxRbuf);
        break;
    case eGSM_IDLE:
        _xInst.eConnectionStatus = eGSM_CONNECTED_TO_BROKER;
        break;
    case eGSM_SEND_USSD:
        _xInst.eConnectionStatus = eGSM_TCP_READY;
        if (_bGSMSendCommWithResp(eGSM_COMM_GET_BALANCE)){
            bAtSwitchToDataMode(&_xInst.xAt);
            _xInst.eStatus = eGSM_IDLE;
        }
        break;
    case eGSM_SEND_SMS:
        _xInst.eConnectionStatus = eGSM_TCP_READY;
        if (_xInst.eCommandState == eGSM_COMMAND_PASSED) {
            _xInst.eCommandState = eGSM_COMMAND_CHECK;
            _xInst.eStatus = eGSM_SEND_SMS2;
        }
        break;
    case eGSM_SEND_SMS2:
        _xInst.eConnectionStatus = eGSM_TCP_READY;
        if (_xInst.eCommandState == eGSM_COMMAND_PASSED && _xInst.xAt.eWaitStatus == eAT_WAIT_NOTHING) {
            if (bAtSwitchToDataMode(&_xInst.xAt)) _xInst.eStatus = eGSM_IDLE;
        }
        break;
    case eGSM_RECEIVE_SMS:
        _xInst.eConnectionStatus = eGSM_TCP_READY;
        if (_xInst.eCommandState == eGSM_COMMAND_PASSED) {
            _xInst.eCommandState = eGSM_COMMAND_CHECK;
            _xInst.eStatus = eGSM_RECEIVE_SMS2;
            cStartTimeout(&_xTimer,30000);
        }
        break;
    case eGSM_RECEIVE_SMS2:
        _xInst.eConnectionStatus = eGSM_TCP_READY;
        if (_xInst.eCommandState == eGSM_COMMAND_PASSED && _xInst.xAt.eWaitStatus == eAT_WAIT_NOTHING) {
           // if (cCheckTimeout(&_xTimer)){
                if (bAtSwitchToDataMode(&_xInst.xAt)) _xInst.eStatus = eGSM_IDLE;
           // }
        }
        break;
    default:
        //do nothing;
        break;
    }

}

void vGSMStatusReset(){
    _xInst.ePower = eGSM_POWER_NULL;
    _xInst.eStatus = eGSM_POWERING;
    _xInst.eConnectionStatus = eGSM_OFFLINE;
    _xInst.eCommandState = eGSM_COMMAND_WAIT;
    _xInst.bResetNeeded = false;
    _xInst.bGotBalance = false;
}
void vGSMPowerReset(){
    _xInst.eStatus = eGSM_POWERING;
}

void vGSMStart(){
    _xInst.eStatus = eGSM_UART_SETUP;
}


void vGSMInit(GSMOptions_x * options){
    vGSMStatusReset();
    if (!options) return;

    cStartTimeout(&_xInst.xCommandTimeout, options->ulTimeout);
    _xInst.ulTimeout = options->ulTimeout;
    vGSMSetApn(options->apnUrl, options->apnUserName, options->apnPassWord);
    vGSMSetHost("TCP", options->hostUrl, options->hostPort);
    vGSMSetHandlers(options->serial, options->set_pk, options->get_st);
}

//Init control pins and UART handler
void vGSMSetHandlers(UART_HandleTypeDef* serial, void (*setPk)(bool), bool (*getSt)(void))
{
    _xInst.eStatus = eGSM_POWERING;
    _xInst.ePower = eGSM_POWER_NULL;
    _xInst.set_pk = setPk;
    _xInst.get_st = getSt;
    bAtInit(&_xInst.xAt, serial, _vGSM_ATHandler);
}

// power Up GPRS Shield
void vGSMResetPower() {
    _xInst.eStatus = eGSM_POWERING;
    _xInst.ePower = eGSM_POWER_NULL;
}


char* bGSMGetIPAddress()
{
    if (!_xInst.acIpString) {
        if (_xInst.xAt.ulIp){
            char ip[AT_IP_SIZE];
            uint8_t octet[4];
            for(int i = 0 ; i < 4 ; i++){
                octet[i] = _xInst.xAt.ulIp >> (i * 8);
            }
            sprintf(ip, "%d.%d.%d.%d",octet[3], octet[2], octet[1], octet[0]);
            strlcpy(_xInst.acIpString, ip, AT_IP_SIZE);
        }
    }
    return _xInst.acIpString;
}

unsigned long ulGSMGetIp()
{
  return _xInst.xAt.ulIp;
}

char * pcaGSMGetIMCI(){
    return _xInst.xAt.caIMSI;
}
void vGSMSetApn(const char* apnUrl, const char* userName, const char* passWord){
    strlcpy(_xInst.xApn.op1, apnUrl, AT_STATIC_FIELD_SIZE);
    strlcpy(_xInst.xApn.op2, userName,AT_STATIC_FIELD_SIZE);
    strlcpy(_xInst.xApn.op3, passWord,AT_STATIC_FIELD_SIZE);
    static char settings[64];
    _GSM_COMMANDS_STRINGS[eGSM_COMM_CSTT] = settings;

    snprintf(_GSM_COMMANDS_STRINGS[eGSM_COMM_CSTT], AT_STATIC_COMMAND_SIZE, "AT+CSTT=\"%s\",\"%s\",\"%s\"\r\n", apnUrl, userName, passWord);
}

void vGSMSetHost(const char* tcp, const char* hostUrl, const char* port){
    strlcpy(_xInst.xHost.op1, tcp, AT_STATIC_FIELD_SIZE);
    strlcpy(_xInst.xHost.op2, hostUrl, AT_STATIC_FIELD_SIZE);
    strlcpy(_xInst.xHost.op3, port, AT_STATIC_FIELD_SIZE);
    static char settings[64];

    _GSM_COMMANDS_STRINGS[eGSM_COMM_CONNEC] = settings;
    snprintf(_GSM_COMMANDS_STRINGS[eGSM_COMM_CONNEC], AT_STATIC_COMMAND_SIZE, "AT+CIPSTART=\"%s\",\"%s\",%s\r\n", tcp, hostUrl, port);
}


size_t xGSMRxAvailable(){
    return rbuf_available(&_xInst.xAt.xRxRbuf);
}


size_t xGSMReadData(uint8_t * buffer, size_t size){
    return rbuf_pop_array(&_xInst.xAt.xRxRbuf, buffer, size);
}

size_t xGSMSendData(uint8_t * buffer, size_t size){
    return rbuf_push_array(&_xInst.xAt.xTxRbuf, buffer, size);
}

void vGSMOnConnect(void (*onConnect)(void)){
    _xInst.onConnect = onConnect;
}

void vGSMOnDisconnect(void (*onDisconnect)(void)){
    _xInst.onDisconnect = onDisconnect;
}
GSMConnStatus_e xGSMGetConnectionStatus(){
    return _xInst.eConnectionStatus;
}

GSMPowerState_e bGSMGetPowerStatus(){
    return _xInst.ePower;
}

bool bGSMGetBalance(int32_t * balance){
    if (!_xInst.bGotBalance) return false;
    *balance = _xInst.xAt.iBalance;
    return true;
}

const char * bGSMGetOperator(){
    return _xInst.xAt.xOperator->name;
}

bool bGSMSendUssd(){
    if (_xInst.eStatus != eGSM_IDLE) return false;

    _xInst.eStatus = eGSM_SEND_USSD;

    return true;
}
/**********************
 * STATIC FUNCTIONS
 **********************/
void _vGSMCheckOk(AT_EVENT_e event){
    switch(event){
    case AT_OK:
        _xInst.eCommandState = eGSM_COMMAND_PASSED;
        break;
    case AT_ERROR:
        _xInst.eCommandState = eGSM_COMMAND_ERROR;
        break;
    case AT_CUSTOM:
    default:
        //Do nothing? //TODO:
        ;; //_xInst.xCommandState = eGSM_COMMAND_ERROR;
        break;
    }
}

void _vGSMShowError(SIM800_ERR_e error){
    switch (error) {
    case SIM800_ERR_OPERATION_NOT_ALLOWED:
        GSM_LOG("Error: SIM800_ERR_OPERATION_NOT_ALLOWED\r\n");
        break;
    case SIM800_ERR_SIM_NOT_INSERTED:
        GSM_LOG("Error: SIM800_ERR_SIM_NOT_INSERTED\r\n");
        break;
    default:
        GSM_LOG("Error: %d\r\n", error);
        break;
    }
}

void _vGSM_ATHandler(AT_EVENT_e event){
    if (_xInst.eCommandState == eGSM_COMMAND_CHECK) {
        _vGSMCheckOk(event);
    }

    switch (event){
    case AT_DISONNECTED:
        if (_xInst.onDisconnect) _xInst.onDisconnect();
        if (_xInst.eStatus == eGSM_IDLE){
            _xInst.eStatus = eGSM_JOINING;
        }
        _xInst.eCommandState = eGSM_COMMAND_WAIT;
        break;
    case AT_BALANCE_CAME:
        _xInst.bGotBalance = true;
        GSM_LOG("Balance: %d.%d\r\n", _xInst.xAt.iBalance / 100, _xInst.xAt.iBalance% 100);
        break;
    case AT_CME_ERROR:
        _vGSMShowError(_xInst.xAt.eLastError);
        break;
    case AT_GOT_OPERATOR:
        vGSMSetApn(_xInst.xAt.xOperator->apn, \
                   _xInst.xAt.xOperator->user,\
                   _xInst.xAt.xOperator->pass);
        GSM_LOG("Operator: %s\r\n", _xInst.xAt.xOperator->name);
        break;
    case AT_POWEROFF:
        vGSMPowerReset();
        break;
    case AT_GOT_SMS:
        GSM_LOG("Got SMS: %s \r\n > %s\r\n", _xInst.xAt.cxSms.number, _xInst.xAt.cxSms.msg);
        break;
    default:
        //do nothing
        break;
    }

}
//TODO: add setState to handle onConnect onDisconnect properly
void _vGSMLoopPowering(){
    assert_param(_xInst.get_st);
    assert_param(_xInst.set_pk);
    static uint32_t last = 0;
    switch(_xInst.ePower){
    case eGSM_POWER_NULL:
        if (!_xInst.get_st()) {
            GSM_LOG("\r\nPOWER IS OFF\r\n");
            vAtResetStatus(&_xInst.xAt);
            _xInst.onDisconnect();
            _xInst.set_pk(true);
            _xInst.ePower =eGSM_POWER_SWITCHING_ON_1;
            last = GETTICK();
        } else {
            _xInst.ePower = eGSM_POWER_ON;
            _xInst.bResetNeeded = true;
        }
        break;
    case eGSM_POWER_SWITCHING_ON_1: {
        if (GETTICK() - last > 1000) {
            GSM_LOG("PK switch\r\n");
            _xInst.set_pk(false);
            _xInst.ePower = eGSM_POWER_SWITCHING_ON_2;
            last = GETTICK();
        }
        break;
    }
    case eGSM_POWER_SWITCHING_ON_2: {
        if (_xInst.get_st()){
            _xInst.ePower = eGSM_POWER_ON;
           // _xInst.set_pk(true);
            //osDelay(2000);
        } else {
            //Checkout timeout to repeat powering one more time
            if (GETTICK() - last > 2000) {
                _xInst.ePower = eGSM_POWER_NULL;
            }
        }
        break;
    }
    case eGSM_POWER_SWITCHING_OFF: break;
    case eGSM_POWER_ON: {
        _xInst.ePower = eGSM_POWER_IDLE;
        dma_uart_flush();
        GSM_LOG("===== POWER ON =====\r\n");
        vGSMStart();
        break;
    }
    case eGSM_POWER_IDLE:{
        static uint8_t bumper = 0;
        if (!_xInst.get_st()) bumper++;
        else bumper = 0;
        if (bumper > GSM_BUMPER_ST_PIN){
            bumper = 0;
            vGSMResetPower();
        }
        break;
    }
    case eGSM_POWER_OFF: break;

    }
}

bool _bGSMSendCommWithResp(GSM_COMMANDS_e command){
    switch (_xInst.eCommandState){
    case eGSM_COMMAND_ERROR:

        _xInst.eCommandState = eGSM_COMMAND_DELAY;
        cRestartTimeout(&_xInst.xCommandTimeout);
        break;
    case eGSM_COMMAND_DELAY:
        if (cCheckTimeout(&_xInst.xCommandTimeout))_xInst.eCommandState = eGSM_COMMAND_WAIT; ;
        break;
    case eGSM_COMMAND_WAIT:
        if (bGSMSendCommand(command)){
            _xInst.eCommandState = eGSM_COMMAND_CHECK;
            cStartTimeout(&_xTimer, GSM_TIMEOUT_VAL);
        }

        // Just logs
        if (_xInst.eCurrentCommand == command) {
            GSM_PRINTF(".");
        } else {
            _xInst.eCurrentCommand = command;
            GSM_LOG("Sending: %s \r\n", _GSM_COMMANDS_STRINGS[command]);
        }
        break;
    case eGSM_COMMAND_PASSED:
        _xInst.eCommandState = eGSM_COMMAND_WAIT;
        return true;
    case eGSM_COMMAND_CHECK:

        break;
    default:
        _xInst.eCurrentCommand = eGSM_COMMAND_ERROR;
        break;
    }
    return false;
}

bool _bGSMDoSequence(GSMSequence_t * seq){
    switch (_xInst.eCommandState){
    case eGSM_COMMAND_ERROR:
        if (_uiErrorAmount > AT_UART_ERROR_LIMIT) {
            seq->i = 0;
        }
        _xInst.eCommandState = eGSM_COMMAND_DELAY;
        cRestartTimeout(&_xInst.xCommandTimeout);
        break;
    case eGSM_COMMAND_DELAY:
        if (cCheckTimeout(&_xInst.xCommandTimeout))_xInst.eCommandState = eGSM_COMMAND_WAIT; ;
        break;
    case eGSM_COMMAND_WAIT:
        if (_xInst.eCurrentCommand == seq->commands[seq->i]) {
            GSM_PRINTF(".");
        } else {
            _xInst.eCurrentCommand = seq->commands[seq->i];
            GSM_LOG("Sending: %s \r\n", _GSM_COMMANDS_STRINGS[seq->commands[seq->i]]);
        }

        _xInst.eCurrentCommand = seq->commands[seq->i];
        if (bGSMSendCommand(seq->commands[seq->i])){
            _xInst.eCommandState = eGSM_COMMAND_CHECK;
            cStartTimeout(&_xTimer, GSM_TIMEOUT_VAL);
        }
        break;
    case eGSM_COMMAND_PASSED:
        _xInst.eCommandState = eGSM_COMMAND_WAIT;
        if (_xInst.eCurrentCommand == seq->commands[seq->i]){
            seq->i++;
            _uiErrorAmount = 0;
            if (seq->i >= seq->size) {
                seq->i =0 ;
                return true;
            }
        }
        break;
    case eGSM_COMMAND_CHECK:
        break;
    default:
        _xInst.eCommandState = eGSM_COMMAND_ERROR;
        break;
    }
    return false;
}


bool _bGSMCheckSoftReset(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->bResetNeeded) {
        if (_bGSMSendCommWithResp(eGSM_COMM_CFUN)) {
            inst->bResetNeeded = false;
            return true;
        };
        return false;
    }
    return true;
}

bool _bGSMCheckCfun(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->xAt.eCfun == eAT_CFUN_UNKNOWN) {
        _bGSMSendCommWithResp(eGSM_COMM_GET_CFUN);
        return false;
    }
    return true;
}

bool _bGSMCheckCpin(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->xAt.ePin == eAT_CPIN_UNKNOWN) {
        _bGSMSendCommWithResp(eGSM_COMM_CPIN);
        return false;
    }
    return true;
}

bool _bGSMCheckCimi(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->xAt.eOpDefinedCode == eNULL_OPERATOR) {
        _bGSMSendCommWithResp(eGSM_COMM_GET_IMSI);
        return false;
    }
    return true;
}

bool _bGSMCheckCgatt(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->xAt.eGatt != eAT_CGATT_ATTACHED) {
        _bGSMSendCommWithResp(eGSM_COMM_CGATT);
        return false;
    }
    return true;
}

bool _bGSMCheckConnection(GSMHandler_t * inst){
    assert_param(inst);
    if (!inst->xAt.bIsConnected) {
        if (cCheckTimeout(&inst->xConnTimeout)) {
            inst->eStatus = eGSM_CONNECTING;
        }
        return false;
    }

    return true;
}

bool _bGSMCheckRssi(GSMHandler_t * inst){
    assert_param(inst);

    if (inst->xAt.ubRssi == 0) {
        _bGSMSendCommWithResp(eGSM_COMM_CSQ);
        return false;
    }
    return true;
}

// power Up GPRS Shield
void bGSMPowerOn() {
    _xInst.eStatus = eGSM_POWERING;
    _xInst.ePower = eGSM_POWER_NULL;
}

bool bGSMSendCommand(GSM_COMMANDS_e command){
    return bAtSendCommand(&_xInst.xAt,_GSM_COMMANDS_STRINGS[command]);
}

bool bGSMSendSms(const char *number, const char* msg) {
    if (_xInst.eStatus != eGSM_IDLE) return false;
    char a[AT_MAX_SMS_LEN];
    sprintf(a, "AT+CMGS=\"%s\"", number);
    _GSM_COMMANDS_STRINGS[eGSM_COMM_CMGS] = a;
    bAtSendSms(&_xInst.xAt, msg);
    if (!bGSMSendCommand(eGSM_COMM_CMGS)) return false;
    _xInst.eCommandState = eGSM_COMMAND_CHECK;
    _xInst.eStatus = eGSM_SEND_SMS;
    return true;
}

bool bGSMReadSms(){
    if (_xInst.eStatus != eGSM_IDLE) return false;
    if (!bGSMSendCommand(eGSM_COMM_CMGL)) return false;
    _xInst.eCommandState = eGSM_COMMAND_CHECK;
    _xInst.eStatus = eGSM_RECEIVE_SMS;
    return true;
}


