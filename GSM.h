/*
 *  TCP Connection sequence sample is below. It is a
 *  non-transparent single connection with manual
 *  data receiving
 *
 */

#ifndef GSM_HANDLER_H_
#define GSM_HANDLER_H_

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 * INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "at.h"
/*********************
 * DEFINES
 *********************/
#ifdef GSM_LOG_ON
#include "logger.h"
#define GSM_LOG simple_log
#define GSM_PRINTF simple_printf
#else
#define GSM_LOG (void)
#define GSM_PRINTF (printf)
#endif



#ifndef AT_UART_ERROR_LIMIT
#define AT_UART_ERROR_LIMIT 10
#endif

#ifndef GSM_BUMPER_ST_PIN
#define GSM_BUMPER_ST_PIN 50
#endif

#ifndef GSM_CONNECTION_TIMEOUT
#define GSM_CONNECTION_TIMEOUT 4000
#endif

#ifndef GSM_TIMEOUT_VAL
#define GSM_TIMEOUT_VAL 3000  // Timeout for response (to check if we are in transparent mode)
#endif

#define GSM_GENERATE_ZERO_ARGUMENT_COMMAND_LIST(X) \
	X(eGSM_COMM_NULL,        "", 0) \
	X(eGSM_COMM_UART,        "AT", 0) \
	X(eGSM_COMM_ATE,         "ATE1", 0) \
	X(eGSM_COMM_ATV,         "ATV1", 0) \
	X(eGSM_COMM_CSQ,         "AT+CSQ", 0) \
	X(eGSM_COMM_ERR_LVL,     "AT+CMEE=1", 0) \
	X(eGSM_COMM_CPIN,        "AT+CPIN?", 0) \
	X(eGSM_COMM_GET_CFUN,    "AT+CFUN?", 0) \
	X(eGSM_COMM_CFUN,        "AT+CFUN=1,1", 0) \
	X(eGSM_COMM_REJECT_CALLS, "AT+GSMBUSY=1", 0) \
	X(eGSM_COMM_CNMI,        "AT+CNMI=2,2,0,0,0", 0) \
	X(eGSM_COMM_CMGF,        "AT+CMGF=1", 0) \
	X(eGSM_COMM_CSCS,        "AT+CSCS=\"GSM\"", 0)\
	X(eGSM_COMM_CGATT,       "AT+CGATT?", 0) \
	X(eGSM_COMM_CLIP,        "AT+CLIP=1", 0) \
	X(eGSM_COMM_CIPST,       "AT+CIPSTATUS", 0) \
	X(eGSM_COMM_CIPSHUT,     "AT+CIPSHUT", 0) \
	X(eGSM_COMM_RXGET,       "AT+CIPRXGET=0", 0) \
	X(eGSM_COMM_CIPMODE,     "AT+CIPMODE=1", 0) \
	X(eGSM_COMM_CSTT,        0, 0) \
	X(eGSM_COMM_CIICR,       "AT+CIICR", 0) \
	X(eGSM_COMM_CIFSR,       "AT+CIFSR", 0) \
	X(eGSM_COMM_CONNEC,      0, 0)\
	X(eGSM_COMM_CIPRXGET,    0, 0) \
	X(eGSM_COMM_GET_BALANCE, "AT+CUSD=1,\"*100#\",0",0)\
	X(eGSM_COMM_SET_COPS,    "AT+COPS=0,2", 0) \
	X(eGSM_COMM_GET_IMSI,    "AT+CIMI", 0) \
    X(eGSM_COMM_CMGS,        "AT+CMGS=\"+7999999999\"", 0)\
    X(eGSM_COMM_CMGL,        "AT+CMGL=\"ALL\"", 0)


#define AT_STATIC_FIELD_SIZE 32
#define AT_STATIC_COMMAND_SIZE 128
#define AT_IP_SIZE 16
/**********************
 * TYPEDEFS
 **********************/
AT_GENERATE_TYPES(GSM_GENERATE_ZERO_ARGUMENT_COMMAND_LIST, COMMANDS, GSM_)
// Container for complex commands that depend on settings and need sprintf
typedef struct {
	char op1[AT_STATIC_FIELD_SIZE];
	char op2[AT_STATIC_FIELD_SIZE];
	char op3[AT_STATIC_FIELD_SIZE];
	char command[AT_STATIC_COMMAND_SIZE];
}GSMTriplet_t;


typedef enum{
	eGSM_OFFLINE,
	eGSM_TCP_CLOSED,
	eGSM_TCP_READY,
	eGSM_CONNECTED_TO_BROKER,
}GSMConnStatus_e;

typedef enum{
	eGSM_NULL = 0,
	eGSM_POWERING,
	eGSM_UART_SETUP,
	eGSM_INITING,
	eGSM_JOINING,
	eGSM_CONNECTING,
	eGSM_CONNECTING2,
	eGSM_IDLE,
	eGSM_SEND_USSD,
	eGSM_SEND_SMS,
	eGSM_SEND_SMS2,
	eGSM_RECEIVE_SMS,
    eGSM_RECEIVE_SMS2,
}GSMMainState_e;

typedef enum{
	eGSM_COMMAND_WAIT,
	eGSM_COMMAND_DELAY,
	eGSM_COMMAND_CHECK,
	eGSM_COMMAND_PASSED,
	eGSM_COMMAND_ERROR,
}GSMCommandState_e;

typedef enum{
	eGSM_POWER_NULL,
	eGSM_POWER_IDLE,
	eGSM_POWER_ON,
	eGSM_POWER_OFF,
	eGSM_POWER_SWITCHING_ON_1,
	eGSM_POWER_SWITCHING_ON_2,
	eGSM_POWER_SWITCHING_OFF,
} GSMPowerState_e;

//TODO: should we use a proper type for the pxUartHandler?
typedef struct{
	char acIpString[16];
	void (*onConnect)(void); // Callback for onConnaect
	void (*onDisconnect)(void);// Callback for onDisconnaect
	void (*set_pk)(bool);    // Set PK pin function
	bool (*get_st)(void);    // Get ST pin function
	GSMTriplet_t xApn;
	GSMTriplet_t xHost;
	GSM_COMMANDS_e eCurrentCommand;
	GSMCommandState_e eCommandState;
	GSMConnStatus_e eConnectionStatus;
	GSMMainState_e eStatus;
	GSMPowerState_e ePower;
	uint32_t ulTimeout;
	GSMTimeout_t xCommandTimeout;
	GSMTimeout_t xConnTimeout;
	ATInst_t xAt;
	bool bResetNeeded;
	bool bGotBalance;
}GSMHandler_t;


typedef struct{
	uint8_t size;
	uint8_t i;
	uint8_t commands[];
}GSMSequence_t;


typedef struct{
	char apnUrl[32];
	char apnUserName[16];
	char apnPassWord[16];
	char hostUrl[32];
	char hostPort[16];
	char mqttUser[32];
	char mqttPass[16];
	uint32_t ulTimeout;
	void * serial;
	void (*set_pk)(bool);
	bool (*get_st)(void);
}GSMOptions_x;

typedef uint8_t (*gsm_callback)(GSMHandler_t* p, int index, const char * input);
/**********************
 * GLOBAL PROTOTYPES
 **********************/
/**
 * Main GSM routine
 */
void vGSMLoop();

/**
 * Reset status to init GSM module
 */
void vGSMReset();

/**
 * Set status to init GSM module
 * @param options tcp options such as like apn, host and so on
 */
void vGSMInit(GSMOptions_x * options);

/** @brief Set Apn params to join GPRS
 *  @param apnUrl
 *  @param userName
 *  @param passWord
 */
void vGSMSetApn(const char* apnUrl, const char* userName, const char* passWord);


/**
 * Set host credentials to connect
 * @param tcp "TCP" or "UDP"
 * @param host host url
 * @param port (as a string)
 */
void vGSMSetHost(const char* tcp, const char* host, const char* port);


/**
 * Set Handlers for periph control;
 * @param serial Serial handler;
 * @param set_pk Function for setting PK pin;
 * @param get_st Function for getting status of device.
 */
void vGSMSetHandlers(UART_HandleTypeDef* serial, void (*set_pk)(bool), bool (*get_st)(void));

/**
 * @brief: Get power status;
 * @return: power status.
 */
GSMPowerState_e bGSMGetPowerStatus();

/**
 * @brief: Reset power status, to check it from the very beginning
 */
void vGSMResetPower();

/**
 * Get the formatted IP address
 * @return Ip address
 */
char* bGSMGetIPAddress();


/** @brief Copy data to the TX buffer
 *  @param buffer to read from
 *  @param size of buffer
 *  @return amount of copied elements
 */
size_t xGSMSendData(uint8_t * buffer, size_t size);

/** @brief Read data from the RX buffer
 *  @param buffer to write to
 *  @param size of buffer
 *  @return amount of copied elements
 */
size_t xGSMReadData(uint8_t * buffer, size_t size);

/** @brief Set onConnect callback
 *  @param onConnect the callback
 */
void vGSMOnConnect(void (*onConnect)(void));

/** @brief Set onDisconnect callback
 *  @param onDisconnect: the callback
 */
void vGSMOnDisconnect(void (*onDisconnect)(void));

/** @brief  Get connection status
 *  @return Actual connection status
 */
GSMConnStatus_e xGSMGetConnectionStatus();

/**
 * @brief: Right balance to pointer, recieve false if balance in undefined yet
 * @param balance: pointer ro write result
 * @return: true if balance is available, false otherwise
 */
bool bGSMGetBalance(int32_t * balance);

/**
 * @brief: Get the name of operator;
 * @return: name of operator.
 */
const char * bGSMGetOperator();

/**
 *
 */
unsigned long ulGSMGetIp();

char * pcaGSMGetIMCI();
size_t xGSMRxAvailable();
bool bGSMSendUssd();
bool bGSMSendCommand(GSM_COMMANDS_e command);
bool bGSMSendSms(const char * number, const char* msg);
bool bGSMReadSms();
#ifdef __cplusplus
}
#endif

#endif /* GSM_HANDLER_H_ */
