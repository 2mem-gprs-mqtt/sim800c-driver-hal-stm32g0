#ifndef GSM_SIM800_H_
#define GSM_SIM800_H_

typedef enum{
	SIM800_ERR_PHONE_FAILURE = 0,
	SIM800_ERR_NO_CONNECTION_TO_PHONE = 1,
	SIM800_ERR_PHONE_ADAPTOR_LINK_RESERVED = 2,
	SIM800_ERR_OPERATION_NOT_ALLOWED = 3,
	SIM800_ERR_OPERATION_NOT_SUPPORTED = 4,
	SIM800_ERR_PH_SIM_PIN_REQUIRED = 5,
	SIM800_ERR_PH_FSIM_PIN_REQUIRED = 6,
	SIM800_ERR_PH_FSIM_PUK_REQUIRED = 7,
	SIM800_ERR_SIM_NOT_INSERTED = 10,
	SIM800_ERR_SIM_PIN_REQUIRED = 11,
	SIM800_ERR_SIM_PUK_REQUIRED = 12,
	SIM800_ERR_SIM_FAILURE = 13,
	SIM800_ERR_SIM_BUSY = 14,
	SIM800_ERR_SIM_WRONG = 15,
	SIM800_ERR_INCORRECT_PASSWORD = 16,
	SIM800_ERR_SIM_PIN2_REQUIRED = 17,
	SIM800_ERR_SIM_PUK2_REQUIRED = 18,
	SIM800_ERR_MEMORY_FULL = 20,
	SIM800_ERR_INVALID_INDEX = 21,
	SIM800_ERR_NOT_FOUND = 22,
	SIM800_ERR_MEMORY_FAILURE = 23,
	SIM800_ERR_TEXT_STRING_TOO_LONG = 24,
	SIM800_ERR_INVALID_CHARACTERS_IN_TEXT_STRING = 25,
	SIM800_ERR_DIAL_STRING_TOO_LONG = 26,
	SIM800_ERR_INVALID_CHARACTERS_IN_DIAL_STRING = 27,
	SIM800_ERR_NO_NETWORK_SERVICE = 30,
	SIM800_ERR_NETWORK_TIMEOUT = 31,
	SIM800_ERR_NETWORK_NOT_ALLOWED_EMERGENCY_CALL_ONLY = 32,
	SIM800_ERR_NETWORK_PERSONALISATION_PIN_REQUIRED = 40,
	SIM800_ERR_NETWORK_PERSONALISATION_PUK_REQUIRED = 41,
	SIM800_ERR_NETWORK_SUBSET_PERSONALISATION_PIN_REQUIRED = 42,
	SIM800_ERR_NETWORK_SUBSET_PERSONALISATION_PUK_REQUIRED = 43,
	SIM800_ERR_SERVICE_PROVIDER_PERSONALISATION_PIN_REQUIRED = 44,
	SIM800_ERR_SERVICE_PROVIDER_PERSONALISATION_PUK_REQUIRED = 45,
	SIM800_ERR_CORPORATE_PERSONALISATION_PIN_REQUIRED = 46,
	SIM800_ERR_CORPORATE_PERSONALISATION_PUK_REQUIRED = 47,
	SIM800_ERR_RESOURCE_LIMITATION = 99,
	SIM800_ERR_UNKNOWN = 100,
	SIM800_ERR_ILLEGAL_MS = 103,
	SIM800_ERR_ILLEGAL_ME = 106,
	SIM800_ERR_GPRS_SERVICES_NOT_ALLOWED = 107,
	SIM800_ERR_PLMN_NOT_ALLOWED = 111,
	SIM800_ERR_LOCATION_AREA_NOT_ALLOWED = 112,
	SIM800_ERR_ROAMING_NOT_ALLOWED_IN_THIS_LOCATION_AREA = 113,
	SIM800_ERR_SERVICE_OPTION_NOT_SUPPORTED = 132,
	SIM800_ERR_REQUESTED_SERVICE_OPTION_NOT_SUBSCRIBED = 133,
	SIM800_ERR_SERVICE_OPTION_TEMPORARILY_OUT_OF_ORDER = 134,
	SIM800_ERR_UNSPECIFIED_GPRS_ERROR = 148,
	SIM800_ERR_PDP_AUTHENTICATION_FAILURE = 149,
	SIM800_ERR_INVALID_MOBILE_CLASS = 150,
	SIM800_ERR_DNS_RESOLVE_FAILED = 160,
	SIM800_ERR_SOCKET_OPEN_FAILED = 161,
	SIM800_ERR_MMS_TASK_IS_BUSY_NOW = 171,
	SIM800_ERR_THE_MMS_DATA_IS_OVERSIZE = 172,
	SIM800_ERR_THE_OPERATION_IS_OVERTIME = 173,
	SIM800_ERR_THERE_IS_NO_MMS_RECEIVER = 174,
	SIM800_ERR_THE_STORAGE_FOR_ADDRESS_IS_FULL = 175,
	SIM800_ERR_NOT_FIND_THE_ADDRESS = 176,
	SIM800_ERR_THE_CONNECTION_TO_NETWORK_IS_FAILED = 177,
	SIM800_ERR_FAILED_TO_READ_PUSH_MESSAGE = 178,
	SIM800_ERR_THIS_IS_NOT_A_PUSH_MESSAGE = 179,
	SIM800_ERR_GPRS_IS_NOT_ATTACHED = 180,
	SIM800_ERR_TCPIP_STACK_IS_BUSY = 181,
	SIM800_ERR_THE_MMS_STORAGE_IS_FULL = 182,
	SIM800_ERR_THE_BOX_IS_EMPTY = 183,
	SIM800_ERR_FAILED_TO_SAVE_MMS = 184,
	SIM800_ERR_IT_IS_IN_EDIT_MODE = 185,
	SIM800_ERR_IT_IS_NOT_IN_EDIT_MODE = 186,
	SIM800_ERR_NO_CONTENT_IN_THE_BUFFER = 187,
	SIM800_ERR_NOT_FIND_THE_FILE = 188,
	SIM800_ERR_FAILED_TO_RECEIVE_MMS = 189,
	SIM800_ERR_FAILED_TO_READ_MMS = 190,
	SIM800_ERR_NOT_M_NOTIFICATION_IND = 191,
	SIM800_ERR_THE_MMS_INCLOSURE_IS_FULL = 192,
	SIM800_ERR_UNKNOWN_2 = 193,
	SIM800_ERR_NO_ERROR = 600,
	SIM800_ERR_UNRECOGNIZED_COMMAND = 601,
	SIM800_ERR_RETURN_VALUE_ERROR = 602,
	SIM800_ERR_SYNTAX_ERROR = 603,
	SIM800_ERR_UNSPECIFIED_ERROR = 604,
	SIM800_ERR_DATA_TRANSFER_ALREADY = 605,
	SIM800_ERR_ACTION_ALREADY = 606,
	SIM800_ERR_NOT_AT_CMD = 607,
	SIM800_ERR_MULTI_CMD_TOO_LONG = 608,
	SIM800_ERR_ABORT_COPS = 609,
	SIM800_ERR_NO_CALL_DISC = 610,
	SIM800_ERR_BT_SAP_UNDEFINED = 611,
	SIM800_ERR_BT_SAP_NOT_ACCESSIBLE = 612,
	SIM800_ERR_BT_SAP_CARD_REMOVED = 613,
	SIM800_ERR_AT_NOT_ALLOWED_BY_CUSTOMER = 614,
	SIM800_ERR_MISSING_REQUIRED_CMD_PARAMETER = 753,
	SIM800_ERR_INVALID_SIM_COMMAND = 754,
	SIM800_ERR_INVALID_FILE_ID = 755,
	SIM800_ERR_MISSING_REQUIRED_P1_2_3_PARAMETER = 756,
	SIM800_ERR_INVALID_P1_2_3_PARAMETER = 757,
	SIM800_ERR_MISSING_REQUIRED_COMMAND_DATA = 758,
	SIM800_ERR_INVALID_CHARACTERS_IN_COMMAND_DATA = 759,
	SIM800_ERR_INVALID_INPUT_VALUE = 765,
	SIM800_ERR_UNSUPPORTED_MODE = 766,
	SIM800_ERR_OPERATION_FAILED = 767,
	SIM800_ERR_MUX_ALREADY_RUNNING = 768,
	SIM800_ERR_UNABLE_TO_GET_CONTROL = 769,
	SIM800_ERR_SIM_NETWORK_REJECT = 770,
	SIM800_ERR_CALL_SETUP_IN_PROGRESS = 771,
	SIM800_ERR_SIM_POWERED_DOWN = 772,
	SIM800_ERR_SIM_FILE_NOT_PRESENT = 773,
	SIM800_ERR_PARAM_COUNT_NOT_ENOUGH = 791,
	SIM800_ERR_PARAM_COUNT_BEYOND = 792,
	SIM800_ERR_PARAM_VALUE_RANGE_BEYOND = 793,
	SIM800_ERR_PARAM_TYPE_NOT_MATCH = 794,
	SIM800_ERR_PARAM_FORMAT_INVALID = 795,
	SIM800_ERR_GET_A_NULL_PARAM = 796,
	SIM800_ERR_CFUN_STATE_IS_0_OR_4 = 797,
	SIM800_ERR_NULL
}SIM800_ERR_e ;


#define AT_GENERATE_CPIN_LIST(X) \
        X(eAT_CPIN_READY, "READY", 0) \
        X(eAT_CPIN_PH_PIN, "PH_SIM PIN", 0) \
        X(eAT_CPIN_PH_PUK, "PH_SIM PUK", 0) \
        X(eAT_CPIN_PIN2, "SIM PIN2", 0) \
        X(eAT_CPIN_PUK2, "SIM PUK2", 0) \
        X(eAT_CPIN_PUK, "SIM PUK", 0) \
        X(eAT_CPIN_PIN, "SIM PIN", 0)  \
        X(eAT_CPIN_UNKNOWN, 0, 0)  \

#define AT_GENERATE_CIPSTART_ANSWERS_LIST(X)\
		X(eAT_CIPS_ALREADY_CONNECT, "ALREADY CONNECT", 0) \
		X(eAT_CIPS_CONNECT_OK, "CONNECT", 0) \
		X(eAT_CIPS_CONNECT_FAIL, "CONNECT FAIL", 0) \
		X(eAT_CIPS_PDP_DEACT, "+PDP: DEACT", 0) \
		X(eAT_CIPS_POWER_DOWN, "NORMAL POWER DOWN", 0) \

#define AT_GENERATE_GPRS_STATUS_LIST(X) \
		X(eAT_GPRS_IP_INITIAL, "IP INITIAL", 0) \
		X(eAT_GPRS_IP_START, "IP START", 0) \
		X(eAT_GPRS_IP_CONFIG, "IP CONFIG", 0) \
		X(eAT_GPRS_IP_GPRSACT, "IP GPRSACT", 0) \
		X(eAT_GPRS_IP_STATUS, "IP STATUS", 0) \
		X(eAT_GPRS_TCP_CONNECTING, "TCP CONNECTING", 0) \
		X(eAT_GPRS_TCP_CLOSING, "TCP CLOSING", 0) \
		X(eAT_GPRS_TCP_CLOSED, "TCP CLOSED", 0) \
		X(eAT_GPRS_PDP_DEACT, "PDP DEACT", 0) \
		X(eAT_GPRS_CLOSED, "CLOSED", 0)

typedef enum {
	eAT_CGATT_DETACHED=0,  // raw bite mode for GPRS data
	eAT_CGATT_ATTACHED=1,  // a default ASCII input
	eAT_CGATT_UNKNOWN=2,
} SIM800_GATT_e;

typedef enum {
	eAT_CFUN_MIN=0,   // Minimum functionality
	eAT_CFUN_FULL=1,  // Full functionality (Default)
	eAT_CFUN_DIS = 4, // Disable phone both transmit and receive RF circuits.
	eAT_CFUN_UNKNOWN = 5
} SIM800_FUN_e;


typedef enum{
	eATV_OK = 0,
	eATV_CONNECT,
	eATV_RING,
	eATV_NO_CARRIER,
	eATV_ERROR,
	eATV_NO_DIALTONE = 6,
	eATV_BUSY,
	eATV_NO_ANSWER,
	eATV_PROCEEDING,
}SIM800_TA_e;

#endif /* GSM_SIM800_H_ */
