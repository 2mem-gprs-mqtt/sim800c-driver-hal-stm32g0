#include "usc2.h"
#include <malloc.h>
#include "stm32_assert.h"
static const char * MEGAFON_BALANCE_PATTERN = "%d.%d";
static const char * TINKOFF_BALANCE_PATTERN = "Баланс %d.%d";

char USC2ParseBalance(char* source, GSMOperatorShort_e operator, int * balance){
    assert_param(source);
    assert_param(balance);
    assert_param(source[0]);
	int ret = 0;
	char decoded[AT_INCOMING_USSD_DATA];
	decoded[0] = 0;
	UCS2ToString(source, decoded, strlen(source));
	int rub = 0;
	int kopeks = 0;

	switch (operator){
	case eMEGAFON:
		ret = sscanf(decoded, MEGAFON_BALANCE_PATTERN, &rub, &kopeks);
		if (ret != 2){
			ret = -2;
		}
		break;
		break;
	case eTINKOFF:
		ret = sscanf(decoded, TINKOFF_BALANCE_PATTERN, &rub, &kopeks);
		if (ret != 2){
			ret = -2;
		}
		break;
	default :
		//Pattern for this operator in undefined!
		ret = -1;
	}

	*balance = rub * 100 + kopeks * (rub >= 0 ? 1 : -1);
	return 0;
}

char UCS2ToString(char* s, char * result, size_t max) {
    assert_param(s);
    assert_param(result);
	char c[5];
	size_t inputSize = strlen(s);

	if (inputSize < 4) return 0;
	if (max < inputSize) return 0;

	for (int i = 0; i < inputSize - 3; i += 4) {
		unsigned long code = (((unsigned int)HexSymbolToChar(s[i])) << 12) +    // �������� UNICODE-��� ������� �� HEX �������������
							 (((unsigned int)HexSymbolToChar(s[i + 1])) << 8) +
							 (((unsigned int)HexSymbolToChar(s[i + 2])) << 4) +
							 ((unsigned int)HexSymbolToChar(s[i + 3]));
		if (code <= 0x7F) {                               // ������ � ������������ � ����������� ���� ��������� ������
		  c[0] = (char)code;
		  c[1] = 0;                                       // �� �������� ��� ����������� ����
		} else if (code <= 0x7FF) {
		  c[0] = (char)(0xC0 | (code >> 6));
		  c[1] = (char)(0x80 | (code & 0x3F));
		  c[2] = 0;
		} else if (code <= 0xFFFF) {
		  c[0] = (char)(0xE0 | (code >> 12));
		  c[1] = (char)(0x80 | ((code >> 6) & 0x3F));
		  c[2] = (char)(0x80 | (code & 0x3F));
		  c[3] = 0;
		} else if (code <= 0x1FFFFF) {
		  c[0] = (char)(0xE0 | (code >> 18));
		  c[1] = (char)(0xE0 | ((code >> 12) & 0x3F));
		  c[2] = (char)(0x80 | ((code >> 6) & 0x3F));
		  c[3] = (char)(0x80 | (code & 0x3F));
		  c[4] = 0;
		}
		strncat(result, c, 4);
	}
	return 1;
}

unsigned char HexSymbolToChar(char c) {
  if      ((c >= 0x30) && (c <= 0x39)) return (c - 0x30);
  else if ((c >= 'A') && (c <= 'F'))   return (c - 'A' + 10);
  else                                 return (0);
}
