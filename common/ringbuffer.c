#include "ringbuffer.h"

static volatile int debug_flag;
int8_t rbuf_init(rbuf_t *c, uint8_t * buffer, size_t size){
	if (!c) return -2;
	c->buffer = buffer;
	c->head = 0;
	c->tail = 0;
	c->maxlen = size;
	return 0;
}

int8_t rbuf_push(rbuf_t *c, uint8_t data)
{
	if (!c) return -2;
    int next;

    next = c->head + 1;  // next is where head will point to after this write.
    if (next >= c->maxlen)
        next = 0;

    if (next == c->tail)  // if the head + 1 == tail, circular buffer is full
        return -1;

    c->buffer[c->head] = data;  // Load data and then move
    c->head = next;             // head to next data offset.
    return 0;  // return success to indicate successful push.
}

int8_t rbuf_pop(rbuf_t *c, uint8_t *data)
{
	if (!c) return -2;
    int next;

    if (c->head == c->tail)  // if the head == tail, we don't have any data
        return -1;

    next = c->tail + 1;  // next is where tail will point to after this read.
    if(next >= c->maxlen)
        next = 0;

    *data = c->buffer[c->tail];  // Read data and then move

    if (*data == '#'){
    	debug_flag = 1;
    } else {
    	debug_flag = 0;
    }

    c->tail = next;              // tail to next offset.
    return 0;  // return success to indicate successful push.
}

size_t rbuf_pop_array(rbuf_t *c, uint8_t * buffer, size_t size){
	size_t i = 0;
	uint8_t res = 0;
	while (i < size ){
		res = rbuf_pop(c, &buffer[i]);
		if (res!= 0) break;
		else i++;
	}
	return i;
}

size_t rbuf_push_array(rbuf_t *c, uint8_t * buffer, size_t size){
	size_t i = 0;
	uint8_t res = 0;
	while (i < size ){
		res = rbuf_push(c, buffer[i]);
		if (res!= 0) break;
		else i++;
	}
	return i;
}

size_t rbuf_available(rbuf_t *c){
	if (c->head >= c->tail){
		debug_flag = 3;
	} else {
		debug_flag = 4;
	}
	return c->head >= c->tail ? c->head - c->tail : c->head + (c->maxlen - c->tail);
}

int8_t rbuf_flush(rbuf_t *c){
	if (!c) return -2;
	c->tail = c->head;
	return 0;
}

size_t rbuf_get_capacity(rbuf_t *c){
	if (c->tail > c->head) {
		return c->tail - c->head - 1;
	} else {
		return c->maxlen - c->head + c->tail - 1;
	}
}

size_t rbuf_get_size(rbuf_t *c){
	return c->maxlen - 1;
}
