#include <dma_uart.h>
#include <string.h>
#include <stdio.h>

static UART_HandleTypeDef *_xUart = NULL;

static uint8_t buffer[AT_UART_LN_BUFFER_SIZE];

static volatile uint16_t circle = 0;
static volatile uint16_t head = 0;
static volatile uint16_t tail = 0;
static volatile bool dmaIsBusy = false;

static GSMTimeout_t timer;
static GSMTimeout_t * pxTimer;
//uint32_t lastTick = 0;


//test
#ifdef TEST_UART
static uint8_t* TEST_BUFFER;

#endif



void dma_uart_init(UART_HandleTypeDef *at_uart, uint32_t sendTimeout){
    _xUart = (UART_HandleTypeDef *)at_uart;
    HAL_UART_Receive_DMA(at_uart, (uint8_t*)&buffer, AT_UART_LN_BUFFER_SIZE);
    head = at_uart->hdmarx->Instance->CNDTR;
    tail = 0;
    dmaIsBusy = false;
    dma_check_tail();
    pxTimer = &timer;
    cStartTimeout(pxTimer, sendTimeout);
    memset(buffer, 0, AT_UART_LN_BUFFER_SIZE);
}


void dma_check_tail(){
	if (tail >= AT_UART_LN_BUFFER_SIZE) {
		tail = 0;
		if (circle) circle--;
	}
}
char dma_get_symbol(){
	dma_refresh_buffer();
	char ret = buffer[tail++];
	dma_check_tail();
	return ret;
}

void dma_refresh_buffer(){
	assert_param(_xUart != NULL);
	if (!_xUart) return;
	head = AT_UART_LN_BUFFER_SIZE - _xUart->hdmarx->Instance->CNDTR;
	if (circle == 1) {
		if (head > tail) tail = head + 1;
		dma_check_tail();
	} else  if (circle >2){
		tail = head + 1;
		dma_check_tail();
	}
}

int dma_uart_rx_available()
{
	dma_refresh_buffer();
	if (circle > 0) {
		return (head - tail + AT_UART_LN_BUFFER_SIZE);
	} else {
		return (head - tail);
	}
	//return CircularBuffer_getUnreadSize(&pRxCircBuf);
}

void dma_uart_flush()
{
	dma_refresh_buffer();
	tail = head;
}

// =================   TX   ===================
//TODO: should it really be unblocking?
void dma_uart_send_data(uint8_t* cmd, size_t len)
{
	#ifdef DMA_UART_UNBLOCKING
	while (dmaIsBusy) { // wait until dma is will finish last task
		if (cCheckTimeout(pxTimer)){
			dmaIsBusy = false;
		}
	}
	cRestartTimeout(pxTimer);
	dmaIsBusy = true;
	HAL_UART_Transmit_DMA(_xUart, (uint8_t *)cmd, len);
	#else
	HAL_UART_Transmit(_xUart, (uint8_t *)cmd, len, pxTimer->uiElapsed);
	#endif
}

void dma_uart_send_byte(uint8_t c)
{
	dma_uart_send_data((uint8_t *)&c, 1);
}

void dma_uart_send_string(const char* cmd)
{
	if (!cmd) return;
	dma_uart_send_data((uint8_t*)cmd, strlen(cmd));
}
// ============================================

// =======  UART INTERRUPT HANDLERS ========
bool dma_uart_rx_callback(void * uart){
	if (_xUart != uart) return false;
	circle++;
	dma_refresh_buffer();
	_xUart->RxState = HAL_UART_STATE_READY;
	return true;
}

bool  dma_uart_tx_callback(void * uart){
	if (_xUart != uart) return false;
	dmaIsBusy = false;
	return true;
}

bool dma_uart_handle_uart_error(void * uart_device){
	if (uart_device != _xUart) return false;
	circle = 0;
    HAL_UART_Receive_DMA(_xUart, (uint8_t *)&buffer, AT_UART_LN_BUFFER_SIZE);
    head = _xUart->hdmarx->Instance->CNDTR;
    dma_check_tail();
    return true;
}

// =========================================

#ifdef TEST_UART
void test_dma_uart_test_echo(){
	int sizeOfTestBuffer = 540;
	TEST_BUFFER = malloc(sizeOfTestBuffer);
	for (int i = 0; i < sizeOfTestBuffer; i++) {
		char a = i % 27;
		if (a < 26) TEST_BUFFER[i] = 'a' + a;
		else TEST_BUFFER[i] = '\n';
	}

	for (int i = 0; i < sizeOfTestBuffer; i++) {
		dma_uart_send_byte(TEST_BUFFER[i]);

	}

	while(1){
		for (int i = 0; i < sizeOfTestBuffer; i++) {
			while (!dma_uart_rx_available()){}
			char sym = dma_get_symbol();
			if (TEST_BUFFER[i] != sym) {
				char  fail[100];
				sprintf(fail, "\rFAIL, i:%d Expecting: %d, Got %d\n", i, TEST_BUFFER[i], sym);
				dma_uart_send_string(fail);
				dma_uart_flush();
				break;
			}
		}
	}
}
#endif


#ifdef TEST
uint8_t* dma_uart_get_buffer(){
	return buffer;
}
#endif
