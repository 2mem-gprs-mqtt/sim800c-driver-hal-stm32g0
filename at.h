#ifndef AT_UART_AT_UART_H_
#define AT_UART_AT_UART_H_
#ifdef __cplusplus
extern "C" {
#endif

 /*********************
 * INCLUDES
 *********************/
#include "dma_uart.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include "common/ringbuffer.h"
#include "common/timeout.h"
#include "common/sim800.h"
#include "common/usc2.h"
#include "common/apn.h"
//#include "common_macros.h"
/*********************
 * DEFINES
 *********************/
#ifndef AT_UART_TX_TIMEOUT
#define AT_UART_TX_TIMEOUT 3000
#endif

#define AT_UART_END_MARK   26


#ifndef GPRS_RX_BUFFER_SIZE
#define GPRS_RX_BUFFER_SIZE SIM800_MAX_RECEIVING_SIZE + 1
#endif

#ifndef GPRS_TX_BUFFER_SIZE
#define GPRS_TX_BUFFER_SIZE SIM800_MAX_RECEIVING_SIZE + 1
#endif

#ifndef AT_UART_EOL
#define AT_UART_EOL "\r\n"
#endif

#ifndef AT_UART_SEND_TIMEOUT_MS
#define AT_UART_SEND_TIMEOUT_MS 2000
#endif

#define AT_UART_DELAYED_COMMAND_SIZE 100
#define AT_UART_TRANSPARENT_TIMEOUT 1000
#define AT_IMSI_LEN 15

#define AT_MAX_SMS_LEN 150
#define AT_PHONE_LENGTH 16
#define AT_DATE_LENGTH 32
#define AT_GET_ENUM(X, STR, CALLBACK) X,
#define AT_GET_STRING(X,STR, CALLBACK) STR,
#define AT_GET_CB(X,STR, CALLBACK) CALLBACK,
#define AT_GET_SIZE(X,STR, CALLBACK) +1

#define AT_GENERATE_RESPONCE_LIST(X) \
        X(eAT_RESPONCE_CIPREXGET, "+CIPRXGET:", _ubAtGetRxSize) \
        X(eAT_RESPONCE_CSQ, "+CSQ:", _ubAtSetSignalLevel) \
		X(eAT_RESPONCE_CGATT, "+CGATT:", _ubAtGetCGATTStatus) \
		X(eAT_RESPONCE_CME_ERROR, "+CME ERROR:", _ubAtGetErrorMsg) \
		X(eAT_RESPONCE_CPIN, "+CPIN:", _ubAtCheckCpin)\
		X(eAT_RESPONCE_GPRS, "STATE:", _ubAtCheckGprsState)\
		X(eAT_RESPONCE_CUSD, "+CUSD:", _ubAtCheckCusd) \
		X(eAT_RESPONCE_COPS, "+COPS:", _ubAtCheckCops) \
		X(eAT_RESPONCE_CFUN, "+CFUN:", _ubAtCheckCfun) \
		X(eAT_RESPONCE_CMT,  "+CMT:",  _ubAtCheckCmt) \
        X(eAT_RESPONCE_CMGL,  "+CMGL:",  _ubAtCheckCmgl) \
		X(eAT_RESPONCE_COMMON, "", _ubAtCheckCommon)

//TODO: call ring!!!
#define AT_GENERATE_SIM_EVENT_LIST(X) \
        X(eAT_SIM_EVENT_RING, "RING", 0)

#define AT_GENERATE_TYPES(GENERATOR, NAME, PREFIX) \
    typedef enum { \
        GENERATOR(AT_GET_ENUM)\
    } PREFIX ## NAME ## _e;

#define AT_GENERATE_LISTS(GENERATOR, NAME, PREFIX) \
    const int PREFIX ## NAME ##_SIZE = GENERATOR(AT_GET_SIZE); \
    char * PREFIX ## NAME ## _STRINGS[GENERATOR(AT_GET_SIZE)] = {GENERATOR(AT_GET_STRING)};\
    at_callback PREFIX ## NAME ## _CBS[GENERATOR(AT_GET_SIZE)] = {GENERATOR(AT_GET_CB)};

/**********************
 * TYPEDEFS
 **********************/
AT_GENERATE_TYPES(AT_GENERATE_CPIN_LIST, CPIN, AT_)
AT_GENERATE_TYPES(AT_GENERATE_GPRS_STATUS_LIST, GPRS, AT_)
AT_GENERATE_TYPES(AT_GENERATE_RESPONCE_LIST, RESPONCE, AT_)
AT_GENERATE_TYPES(AT_GENERATE_SIM_EVENT_LIST, SIM_EVENT, AT_)
AT_GENERATE_TYPES(AT_GENERATE_CIPSTART_ANSWERS_LIST, CIPS, AT_)


// Possible events from SIM800
typedef enum {
	AT_UNKNOWN = 0,
	AT_ASSERT,
	AT_OK,
	AT_SEND_OK,
	AT_ERROR,
	AT_CUSD_CAME,
	AT_GOT_OPERATOR,
	AT_BALANCE_CAME,
	AT_CME_ERROR,
	AT_GOT_STATUS,
	AT_GOT_SMS_INDICATION,
    AT_GOT_SMS,
	AT_CUSTOM,
	AT_CPIN_READY,
	AT_INCOMING_DATA,
	AT_CPIN_NOT_INSERTED,
	AT_CONNECTED,
	AT_RECONENCT,
	AT_DISONNECTED,
	AT_POWEROFF
} AT_EVENT_e;

// Input mode. AT_MODE_CHAR is a default ASCII input
typedef enum {
	eAT_MODE_COMMAND,               // Waiting for new commands in ASCII from SIM800
	eAT_MODE_WAIT_ANSWER,
	eAT_MODE_DATA,
	eAT_MODE_TRANSPARENT_HOLD,
	eAT_MODE_TRANSPARENT_HOLD2,
}AT_MODE_e;

// if we wait for '>' to send sms, or incoming data.
typedef enum {
    eAT_WAIT_NOTHING = 0,
    eAT_WAIT_SEND_SYMBOL,
    eAT_WAIT_SMS_INCOMING,
    eAT_WAIT_IP,
    eAT_WAIT_IMSI,
}AT_WAIT_ANSWER_e;

typedef struct {
    char msg[AT_MAX_SMS_LEN];
    char number[AT_PHONE_LENGTH];
    char date[AT_DATE_LENGTH];
}AtSms_t;

// This struct holds incoming line;
typedef struct {
	uint16_t i;                             // Current cursor
	uint16_t sum;                           // Amount of matched simbols for seeking pattern (\r\n by default)
	char baBuf[AT_UART_LN_BUFFER_SIZE];     // Buffer that holds parcing line
}RxLine_t;

typedef struct {
	void (*at_event_handler)(AT_EVENT_e);   // Event handler
	//RxLine_t xRxLine;                      // Incoming line buffer
	UART_HandleTypeDef *at_uart;            // Hardware UART handler
	AT_MODE_e at_mode;                      // TCP/Command mode. Look at AT_MODE_e specification
	uint16_t usTcpRxAvailable;               // Amount of data in SIM800 that we may request (we haven't done it yet)
	uint16_t usTcpRxLength;                 // Amount of incoming data from SIM800 that is already requested

	uint8_t baRxBuf[GPRS_RX_BUFFER_SIZE];   // Statically allocated memory for a RX ringbuffer
	rbuf_t xRxRbuf;                         // Ringbuffer with TCP RX data
	uint8_t baTxBuf[GPRS_TX_BUFFER_SIZE];   // Statically allocated memory for a TX ringbuffer
	rbuf_t xTxRbuf;                         // Ringbuffer with TCP TX data

	//SIM800 statuses
	SIM800_GATT_e eGatt;                       // GATT status
	AT_CPIN_e ePin;                         // PIN status
	AT_GPRS_e eGprs;                        // GPRS status
	SIM800_FUN_e eCfun;                     // CFUN status
	uint8_t ubRssi;
	uint8_t ubBer;
	uint32_t ulIp;                           // Local ip address
	size_t xTxAvailable;                    //
	uint8_t prgDataToSend[GPRS_TX_BUFFER_SIZE];
	GSMTimeout_t xTxTimeout;
	GSMTimeout_t xCommandTimeout;
	GSMTimeout_t xTransparenTimeout;
	SIM800_ERR_e eLastError;
	int32_t iBalance;
	uint32_t uiCops;
	GSMOperatorShort_e eOpDefinedCode;
	const GSMOperator_x * xOperator;
	char caDelayedCommand[AT_UART_DELAYED_COMMAND_SIZE];
	char caIMSI[AT_IMSI_LEN+1];
	char caSmsToSend[AT_MAX_SMS_LEN];
	AtSms_t cxSms;
	AT_WAIT_ANSWER_e eWaitStatus;
	bool bIsConnected;
} ATInst_t;

typedef uint8_t (*at_callback)(ATInst_t* p, int index, const char * input);
/**********************
 * GLOBAL MACROS
 **********************/
/**********************
 * GLOBAL VARIABLES
 **********************/
/**********************
 * GLOBAL PROTOTYPES
 **********************/
/*
 *  Init UART with preferred UART handler
 *  @param inst: instance of At handler to init
 *  @param UART: uartDevice handler
 */
bool  bAtInit(ATInst_t* inst, void * uart_device, void (*eventHandler)(AT_EVENT_e));

/*
 *  @brief:Main routine for UART parsing. Read byte, read line, switch at_uart modes
 */
void  vAtLoop(ATInst_t * inst);

void vAtResetStatus(ATInst_t* inst);

AT_CPIN_e eAtGetCPIN(ATInst_t * inst);
AT_GPRS_e eAtGetGprsState(ATInst_t * inst);


/*
 * @brief: parse incoming line to find out single-digit ATV mode (ATV0)
 */
AT_EVENT_e eAtCheckATV(const char * line);

/*
 * @brief: handle a new line
 */
AT_EVENT_e eAtHandleNewLine(ATInst_t * inst, RxLine_t* line);


/**
 * Parse incoming line with Respone list and do callbacks in case of match
 *
 * @param inst: instance to perform a callback with
 * @param line: incoming line to parse
 * @return AT_EVENT_e event
 */
AT_EVENT_e eAtParseLine(ATInst_t * inst, char * line);

/**
 * Parse incoming line with Respone list and do callbacks in case of match
 *
 * @param inst: instance to perform a callback with
 * @param input: incoming line to parse
 * @return AT_EVENT_e event
 */
AT_EVENT_e eAtParseLineSystem(ATInst_t* inst, const char * input);

/**
 *  @brief: Read one byte, call parser and return true in case of EOL
 *  @param inst: instance of At handler to use at_mode;
 *  @param line: a struct with buffer to save incoming data;
 *  @return: bAtReadLine's result;
 *
 *  This function implements a wrapper that reads data from dma_uart and
 *  calls bAtParseSymbol. It's made for a simplier testing. We can use test
 *  symbol parser without mocking the dma_uart.
 */
bool bAtReadLine(ATInst_t * inst, RxLine_t * line);

/**
 *  @brief: Take char, save it to line, do all parsing logic and return true if case of EOL
 *  @param inst: instance of At handler to use at_mode;
 *  @param line: a struct with buffer to save incoming data;
 *  @param c   : a char to parse;
 *  @return: true if we met EOL sequence, false in all other cases
 *
 *  This function looks for defined EOL (end of line) sequence and return true in case
 *  success. Also it waits for '>' to send TCP data. If we wait for incoming TCP data
 *  (eAT_MODE_RX_TCP) we save data to RX ringbuffer here.
 */
bool bAtParseSymbol(ATInst_t * inst, RxLine_t * line, char c);

/**
 * brief: Check current status and send command if everything is ok;
 * @param inst: instanse to proceed with;
 * @param command: a command to send (should be with EOL to have an effect).
 */
bool bAtSendCommand(ATInst_t * inst, const char* command);

/**
 * @brief: Routine for switching mode transparent->command;
 * @param inst: Instance to proceed with;
 * @return: true if we switched to command mode, false in all other cases.
 *
 * This command is called in atLoop. vAtSwitchToCommand should be called to get an effect.
 */
bool bAtWaitCommandMode(ATInst_t * inst);
/**
 *  @brief: Switch at_mode from transparent to command mode;
 *  @param inst: instance of At handler to use at_mode;
 *  @param command: send this command in case of successfull switch;
 *  @return: true if switching has started, return false if current state is invalid for switching;
 *
 *  To switch mode from TCP TRANSPARENT command we should delay for 1 sec, then send `+++`
 *  and then delay again for 1 second. This why we should deny all attempts to write anything to
 *  UART for this time.
 *
 *  To proceed the process of switching we should call bAtWaitCommandMode (if it's not called in AtLoop)
 */
bool bAtSwitchToCommandMode(ATInst_t * inst, const char* command);

/**
 *  @brief: Switch at_mode from command to data mode;
 *  @param inst: instance of At handler to use at_mode;
 *  @return: true if command was sent
 */
bool bAtSwitchToDataMode(ATInst_t * inst);

/**
 * @brief: Check if provided string is an IP "x.x.x.x", 0 <= x <= 255
 * @param s: string to check
 * @param ip: pointer to ul variable to save the result in binary mode
 * @return: true if match, false otherwise
 */
bool validateIP4Dotted(const char *s, uint32_t * ip);

/**
 * @brief: Send sms;
 * @param inst: instance of AT handler to process with;
 * @param sms: sms message;
 * @return: true if sending started, false otherwise.
 */
bool bAtSendSms(ATInst_t * inst, const char * sms);
/**********************
 *  TEST FUNCTIONS
 **********************/
#ifdef TEST
bool test_bAtReadSymbol();
uint8_t test_vAtGetCGATTStatus(ATInst_t* p, int index, char * input);
uint8_t test_vAtGetRxSize(ATInst_t* p, int index, const char * input);
uint8_t test_ubAtGetCFUNStatus(ATInst_t* p, int index, const char * input);
uint8_t test_ubAtSetSignalLevel(ATInst_t* p,int index, const char * input);
ATInst_t * test_pxGetInst();
bool test_bAtCheckIfIsIp(ATInst_t * inst, char* str);
bool validateIP4Dotted(const char *s, uint32_t * ip);
/** @brief Convert string IP to uintt32
 * @param str - sctring to convert
 * @return converted IP
 */
uint32_t ulAtStrToIp(const char* str);

#endif
#ifdef __cplusplus
}
#endif
#endif /* AT_UART_AT_UART_H_ */
