#ifndef GSM_APN_H_
#define GSM_APN_H_

#include <stddef.h>

typedef enum{
    eNULL_OPERATOR,
	eUNDEF_OP,
	eMEGAFON,
	eTINKOFF,
}GSMOperatorShort_e;

typedef struct{
	const char * name;
	int MCC_MNC;
	const char * apn;
	const char * user;
	const char * pass;
	GSMOperatorShort_e eOp;
} GSMOperator_x;



#define OPERATOR_GET_STRUCT(NAME, CODE, APN, USER, PASS, eOP) \
{  .apn=APN,   \
   .name=NAME, \
   .MCC_MNC = CODE, \
   .user = USER, \
   .pass = PASS,\
   .eOp = eOP,\
} ,

#define OPERATOR_TABLE_SIZE(NAME, CODE, APN, USER, PASS, eOP) +1

#define OPERATOR_TABLE(X) \
	X("MTS Mobile TeleSystems",    25001, "internet.mts.ru"    ,     "mts",     "mts", eUNDEF_OP) \
	X("MegaFon PJSC",              25002, "internet"           ,        "",        "", eMEGAFON) \
	X("SkylinkCJSC",               25006, "www"                ,        "",        "", eUNDEF_OP)  \
	X("Vainah Telecom CS",         25008, "internet"           ,        "",        "", eUNDEF_OP) \
	X("Skylink Khabarovsky",       25009, "www"                ,        "",        "", eUNDEF_OP) \
	X("Yota Scartel	",             25011, "internet.yota"      ,        "",        "", eUNDEF_OP) \
	X("Miatel",                    25016, "www"                ,        "",        "", eUNDEF_OP) \
	X("Tele2 Tele2	",             25020, "internet.tele2.ru"  ,        "",        "", eUNDEF_OP) \
	X("GlobalTel JSC",             25021, "www"                ,        "",        "", eUNDEF_OP) \
	X("Vainakh Telecom	",         25022, "internet"           ,        "",        "", eUNDEF_OP) \
	X("Thuraya GTNT	",             25023, "www"                ,        "",        "", eUNDEF_OP) \
	X("Letai Tattelecom	",         25027, "www"                ,        "",        "", eUNDEF_OP) \
	X("Iridium",                   25029, "www"                ,        "",        "", eUNDEF_OP) \
	X("Win Mobile K-Telecom",      25032, "www"                ,        "",        "", eUNDEF_OP) \
	X("Sevmobile Sevtelekom",      25033, "www"                ,        "",        "", eUNDEF_OP) \
	X("Krymtelekom",               25034, "www"                ,        "",        "", eUNDEF_OP) \
	X("MOTIV EKATERINBURG-2000",   25035, "www"                ,        "",        "", eUNDEF_OP) \
	X("MTS Bezlimitno.ru",         25050, "www"                ,        "",        "", eUNDEF_OP) \
	X("Volna mobile	KTK Telecom	", 25060, "www"                ,        "",        "", eUNDEF_OP) \
	X("Tinkoff Mobile",            25062, "m.tinkoff"          ,        "",        "", eTINKOFF)  \
	X("Beeline OJSC Vimpel",       25099, "internet.beeline.ru", "beeline", "beeline", eUNDEF_OP) \
	X("Operator is undefined",     0    , "www"                ,        "",        "", eUNDEF_OP) \

const GSMOperator_x * getOperator(int mmc_mnc);

#endif /* GSM_APN_H_ */
