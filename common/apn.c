#include "apn.h"

//GSMOperator_x russian_operators [OPERATOR_TABLE(OPERATOR_TABLE_SIZE)] = {OPERATOR_TABLE(OPERATOR_GET_STRUCT)};
static const GSMOperator_x russian_operators [OPERATOR_TABLE(OPERATOR_TABLE_SIZE)] = {OPERATOR_TABLE(OPERATOR_GET_STRUCT)};

const GSMOperator_x * getOperator(int mcc_mnc){
	if (mcc_mnc < 0) return NULL;

	for (int i = 0 ; i < OPERATOR_TABLE(OPERATOR_TABLE_SIZE) ; i++){
		if (russian_operators[i].MCC_MNC == mcc_mnc) return &russian_operators[i];
	}
	return NULL;
}
