#ifndef GSM_TIMEOUT_H_
#define GSM_TIMEOUT_H_



#ifdef MQTT_TASK
#include "cmsis_os.h"
#define GSM_TIMER_GET_TICK() osKernelSysTick()
#else
#include "stm32g0xx_hal.h"
#define GSM_TIMER_GET_TICK() HAL_GetTick()
#endif
typedef struct {
	uint32_t uiStartTime;
	uint32_t uiElapsed;
}GSMTimeout_t;

char cCheckTimeout(GSMTimeout_t * timer);
char cStartTimeout(GSMTimeout_t * timer, unsigned int timeout);
char cRestartTimeout(GSMTimeout_t * timer);
#endif /* GSM_TIMEOUT_H_ */
