#ifndef GSM_USC2_H_
#define GSM_USC2_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "apn.h"
#ifndef USC2_TRASH_SIZE
#define USC2_TRASH_SIZE 500
#endif

#define AT_INCOMING_USSD_DATA 500

/*
 * @brief: take a string in USC2 format and find a balance value
 * @param source: USC2 string
 * @param operator: Mobile operator (provider)
 * @param balnce: pointer to output variable
 * @return: 0 if succeed, -1 if operator is wrong, -2 if nothing was found
 */
char USC2ParseBalance(char* source, GSMOperatorShort_e operator, int * balance);
unsigned char HexSymbolToChar(char c);
char UCS2ToString(char * s, char * result, size_t max);
#endif /* GSM_USC2_H_ */
