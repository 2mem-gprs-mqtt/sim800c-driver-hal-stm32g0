/*
 * Lowest level of uart for AT commands. It just can send/receive any data and handle interrupts
 */
#ifndef GSM_DMA_UART_H_
#define GSM_DMA_UART_H_

#include "stm32g0xx_hal.h"
#include <stdint.h>
#include <stdbool.h>
#include "common/timeout.h"

#ifdef MQTT_TASK
#include "cmsis_os.h"
#define GETTICK osKernelSysTick
#else
#define GETTICK HAL_GetTick
#endif

#define SIM800_MAX_RECEIVING_SIZE 1456

#ifndef AT_UART_LN_BUFFER_SIZE
#define AT_UART_LN_BUFFER_SIZE 5000
#endif
/**
 * Init UART
 * @param at_uart: huart handler
 * @param sendTimeout: timeout
 */
void dma_uart_init(UART_HandleTypeDef *at_uart, uint32_t sendTimeout);
/**
 * Check registers
 */
void dma_refresh_buffer();
/**
 * Check if tail overflowed
 */
void dma_check_tail();

/**
 * Get next symbol from UART buffer
 * @return next symbol from buffer
 */
char dma_get_symbol();

/**
 * A callback for rx complete
 * @param uart: Serial handler
 * @return : true if uart is correct and event was handled
 */
bool dma_uart_rx_callback(void * uart);
/**
 * A callback for tx complete
 * @param uart
 * @return true if uart is correct and event was handled
 */
bool dma_uart_tx_callback(void * uart);

/**
 * Handle any uart error
 * @param uart_device
 * @return true if uart is correct and event was handled
 */
bool dma_uart_handle_uart_error(void * uart_device);

/**
 * Get amount of available symbols to read
 * @return amount of available symbols
 */
int  dma_uart_rx_available();

/**
 * Move tail to head and "flush" data
 */
void dma_uart_flush();

/**
 * Send a byte by UART
 * @param data
 */
void dma_uart_send_byte(uint8_t data);

/**
 * Send a string by uart (should be null-terminated!)
 * @param cmd a null-terminated string
 */
void dma_uart_send_string(const char* cmd);

/**
 * Send data of defined size
 * @param data
 * @param size
 */
void dma_uart_send_data(uint8_t* data, size_t size);

#ifdef TEST
/**
 * Test function
 */
void test_dma_uart_test_echo();

uint8_t* dma_uart_get_buffer();
#endif
#endif /* GSM_DMA_UART_H_ */
