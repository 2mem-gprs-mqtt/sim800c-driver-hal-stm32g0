#include "timeout.h"

char cCheckTimeout(GSMTimeout_t * timer){
	GSMTimeout_t * pxTimer = timer;
	if (!pxTimer) return 0;


	if (GSM_TIMER_GET_TICK() - pxTimer->uiStartTime > pxTimer->uiElapsed){
		return 1;
	} else {
		return 0;
	}
}

char cStartTimeout(GSMTimeout_t * timer, unsigned int timeout){
	GSMTimeout_t * pxTimer = timer;
	if (!pxTimer) return 0;

	pxTimer->uiStartTime = GSM_TIMER_GET_TICK();
	pxTimer->uiElapsed = timeout;
	return 1;
}

char cRestartTimeout(GSMTimeout_t * timer){
	GSMTimeout_t * pxTimer = timer;
	if (!pxTimer) return 0;

	pxTimer->uiStartTime = GSM_TIMER_GET_TICK();
	return 0;
}
