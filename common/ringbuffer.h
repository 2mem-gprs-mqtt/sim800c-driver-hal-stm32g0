#ifndef GSM_RINGBUFFER_H_
#define GSM_RINGBUFFER_H_

#include <stdint.h>
#include <stddef.h>

//TODO: unsafe code?
typedef struct {
    uint8_t * buffer;
    uint32_t head;
    uint32_t tail;
    int maxlen;
} rbuf_t;

//Push data to ring buffer
int8_t rbuf_push(rbuf_t *c, uint8_t data);

//Pop data from buffer
int8_t rbuf_pop(rbuf_t *c, uint8_t *data);

/** Pop array of data from buffer
 *  @param c buffer to read from
 *  @param buffer buffer
 *  @param size of elemnts to read
 *  @return amount of poped elements (could be less than size)
 */
size_t rbuf_pop_array(rbuf_t *c, uint8_t * buffer, size_t size);

/** Push array of data to buffer
 *  @param c buffer to write to
 *  @param buffer buffer
 *  @param  size of elemnts to write
 *  @return amount of pushed elements (could be less than size)
 */
size_t rbuf_push_array(rbuf_t *c, uint8_t * buffer, size_t size);

/**
 * Init buffer
 * @param c : Ringbuffer to init
 * @param buffer : buffer for rbuf
 * @param size
 * @return
 */
int8_t rbuf_init(rbuf_t *c, uint8_t * buffer, size_t size);


/** How much elements is available
 *  @param c: pointer to buffer
 *  @return amount of available elements
 */
size_t rbuf_available(rbuf_t *c);

/** Move tail to head (empty the buffer)
 *  @param c: pointer to buffer
 *  @return result
 */
int8_t rbuf_flush(rbuf_t *c);

/** Return amount of empty space
 *  @param c: pointer to buffer
 *  @return available capacity
 */
size_t rbuf_get_capacity(rbuf_t *c);

/** Return limit of empty space
 *  @param c: pointer to buffer
 *  @return max capacity
 */
size_t rbuf_get_size(rbuf_t *c);
#endif /* GSM_RINGBUFFER_H_ */
